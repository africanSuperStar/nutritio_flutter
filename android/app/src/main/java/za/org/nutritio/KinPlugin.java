package za.org.nutritio;

import android.app.Application;
import android.app.Activity;
import android.app.Dialog;
import android.os.StrictMode;
import android.os.StrictMode.VmPolicy;
import android.util.Log;

import androidx.annotation.NonNull;

import java.util.concurrent.TimeUnit;

import kin.sdk.Environment;
import kin.sdk.KinAccount;
import kin.sdk.KinClient;
import kin.sdk.exception.CreateAccountException;
import okhttp3.OkHttpClient;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;

public class KinPlugin extends Application implements MethodCallHandler {

    public enum NetWorkType {
        MAIN,
        TEST
    }

    private static final String CHANNEL = "za.org.nutritio/kin";

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int APP_INDEX = 0;
    private static final String STUB_APP_ID = "nfs1";

    private OkHttpClient okHttpClient;
    private KinClient kinClient;
    private KinAccount account;

    /** Plugin registration. */
    Activity context;
    MethodChannel methodChannel;

    public static void registerWith(Registrar registrar) {
        final MethodChannel channel =
                new MethodChannel(registrar.messenger(), CHANNEL);

        channel.setMethodCallHandler(new KinPlugin(registrar.activity(), channel));
    }

    public KinPlugin(Activity activity, MethodChannel methodChannel) {
        this.context = activity;
        this.methodChannel = methodChannel;
        this.methodChannel.setMethodCallHandler(this);
    }

    public interface Callbacks {
        void onSuccess();
        void onFailure(Exception e);
    }

    private void fireOnFailure(@NonNull Callbacks callbacks, Exception ex) {
        callbacks.onFailure(ex);
    }

    private void fireOnSuccess(@NonNull Callbacks callbacks) {
        callbacks.onSuccess();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        StrictMode.setVmPolicy(new VmPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .build());

        okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .build();

        // Kin client is the manager for Kin accounts
        kinClient = new KinClient(
                this,
                Environment.TEST,
                STUB_APP_ID,
                "nutritio"
        );
    }

    public KinClient getKinClient() {
        return kinClient;
    }

    @Override
    public void onMethodCall(MethodCall call, Result result) {

        // Arguments
        if (call.arguments.equals("kinClient")) {

            if (kinClient != null) {
                result.success(kinClient);
            } else {
                result.error("DOES_NOT_EXIST", "Kin Client does not exist.", null);
            }
        }

        // Methods
        if (call.method.equals("getKinAccount")) {
            KinAccount kinAccount = getKinAccount(APP_INDEX);

            if (kinAccount != null) {
                result.success(kinAccount);
            } else {
                Dialog dialog=new Dialog(context);
                dialog.setTitle("DOES_NOT_EXIST: Kin Account does not exist.");
                dialog.show();
            }
        } else if (call.method.equals("createKinAccount")) {
            createKinAccount();

            if (this.account != null) {
                result.success(null);
            } else {
                Dialog dialog=new Dialog(context);
                dialog.setTitle("FAILED_CREATE_ACCOUNT: Failed to create a new Kin Account.");
                dialog.show();
            }
        } else {
            result.notImplemented();
        }
    }

    public void createKinAccount() {
        try {
            if (!kinClient.hasAccount()) {
                this.account = kinClient.addAccount();
            }
        } catch (CreateAccountException e) {
            e.printStackTrace();
        }
    }

    public KinAccount getKinAccount(int index) {
        // The index that is used to get a specific account from the client manager
        KinAccount kinAccount = kinClient.getAccount(index);
        try {
            // Creates a local keypair
            if (kinAccount == null) {
                kinAccount = kinClient.addAccount();
                Log.d(TAG, "Created new account succeeded");
            }
        } catch (CreateAccountException e) {
            e.printStackTrace();
        }

        return kinAccount;
    }
}

