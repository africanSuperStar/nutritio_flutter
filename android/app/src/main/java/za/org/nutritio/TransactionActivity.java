package za.org.nutritio;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.widget.EditText;

import java.util.concurrent.TimeUnit;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;

import io.flutter.app.FlutterActivity;
import io.flutter.plugin.common.MethodChannel;
import kin.sdk.Transaction;
import kin.sdk.TransactionId;
import kin.utils.Request;
import okhttp3.OkHttpClient;

/**
 * Displays form to enter public address and amount and a button to send a transaction
 */
//public class TransactionActivity extends FlutterActivity implements MethodCallHandler {
//
//    public static final String TAG = TransactionActivity.class.getSimpleName();
//
//    public static Intent getIntent(Context context) {
//        return new Intent(context, TransactionActivity.class);
//    }
//
//    private EditText toAddressInput, amountInput, feeInput, memoInput;
//    private Request<Long> gertMinimumFeeRequest;
//    private Request<Transaction> buildTransactionRequest;
//    private Request<TransactionId> sendTransactionRequest;
//    private WhitelistService whitelistService;
//
//    public interface WhitelistServiceCallbacks {
//        void onSuccess(String whitelistTransaction);
//        void onFailure(Exception e);
//    }
//
//    private static final String CHANNEL = "za.org.nutritio/whitelist";
//
//    /** Plugin registration. */
//    Activity context;
//    MethodChannel methodChannel;
//
//    public static void registerWith(Registrar registrar) {
//        final MethodChannel channel =
//                new MethodChannel(registrar.messenger(), CHANNEL);
//
//        channel.setMethodCallHandler(new KinPlugin(registrar.activity(), channel));
//    }
//
//    public WhitelistService(Activity activity, MethodChannel methodChannel) {
//        handler = new Handler(Looper.getMainLooper());
//        okHttpClient = new OkHttpClient.Builder()
//                .connectTimeout(20, TimeUnit.SECONDS)
//                .readTimeout(20, TimeUnit.SECONDS)
//                .build();
//
//        this.context = activity;
//        this.methodChannel = methodChannel;
//        this.methodChannel.setMethodCallHandler(this);
//    }
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        whitelistService = new WhitelistService();
//        initWidgets();
//    }
//}