// package za.org.nutritio;

// import android.app.Activity;
// import android.app.Dialog;
// import android.os.Handler;
// import android.os.Looper;

// import androidx.annotation.NonNull;

// import java.io.IOException;
// import java.util.concurrent.TimeUnit;

// import kin.sdk.WhitelistableTransaction;
// import okhttp3.Call;
// import okhttp3.Callback;
// import okhttp3.MediaType;
// import okhttp3.OkHttpClient;
// import okhttp3.Request;
// import okhttp3.RequestBody;
// import okhttp3.Response;
// import org.json.JSONException;

// import io.flutter.plugin.common.MethodCall;
// import io.flutter.plugin.common.MethodChannel;
// import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
// import io.flutter.plugin.common.MethodChannel.Result;
// import io.flutter.plugin.common.PluginRegistry.Registrar;

// public class WhitelistService implements MethodCallHandler {

//     private static final String URL_WHITELISTING_SERVICE = "http://34.239.111.38:3000/whitelist";
//     private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

//     private final OkHttpClient okHttpClient;
//     private final Handler handler;

//     private static final String CHANNEL = "za.org.nutritio/whitelist";

//     /** Plugin registration. */
//     Activity context;
//     MethodChannel methodChannel;

//     public static void registerWith(Registrar registrar) {
//         final MethodChannel channel =
//                 new MethodChannel(registrar.messenger(), CHANNEL);

//         channel.setMethodCallHandler(new KinPlugin(registrar.activity(), channel));
//     }

//     public WhitelistService(Activity activity, MethodChannel methodChannel) {
//         handler = new Handler(Looper.getMainLooper());
//         okHttpClient = new OkHttpClient.Builder()
//                 .connectTimeout(20, TimeUnit.SECONDS)
//                 .readTimeout(20, TimeUnit.SECONDS)
//                 .build();

//         this.context = activity;
//         this.methodChannel = methodChannel;
//         this.methodChannel.setMethodCallHandler(this);
//     }

//     @Override
//     public void onMethodCall(MethodCall methodCall, Result result) {

//         // Methods
//         if (methodCall.method.equals("whitelistTransaction")) {
//             whitelistTransaction();


//         } else {
//             result.notImplemented();
//         }
//     }

//     void whitelistTransaction(WhitelistableTransaction whitelistableTransaction,
//                               TransactionActivity.WhitelistServiceListener whitelistServiceListener) throws JSONException {
//         RequestBody requestBody = RequestBody.create(JSON, toJson(whitelistableTransaction));
//         Request request = new Request.Builder()
//                 .url(URL_WHITELISTING_SERVICE)
//                 .post(requestBody)
//                 .build();
//         okHttpClient.newCall(request)
//                 .enqueue(new Callback() {
//                     @Override
//                     public void onFailure(@NonNull Call call, @NonNull IOException e) {
//                         if (whitelistServiceListener != null) {
//                             fireOnFailure(whitelistServiceListener, e);
//                         }
//                     }

//                     @Override
//                     public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
//                         handleResponse(response, whitelistServiceListener);
//                     }
//                 });
//     }

//     private void fireOnFailure(TransactionActivity.WhitelistServiceListener whitelistServiceListener, Exception e) {
//         handler.post(() -> whitelistServiceListener.onFailure(e));
//     }

//     private void fireOnSuccess(TransactionActivity.WhitelistServiceListener whitelistServiceListener, String response) {
//         handler.post(() -> whitelistServiceListener.onSuccess(response));
//     }
// }
