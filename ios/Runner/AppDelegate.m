#include "AppDelegate.h"
#include "GeneratedPluginRegistrant.h"

#import "Runner-Swift.h"

@interface InitializeKin : NSObject
+ (KinSDKManager *)init:(UINavigationController *)controller with:(FlutterViewController *)flutterViewController;
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application
    didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{

//    InitArgs(CommandLine.argc, CommandLine.unsafeArgv)
    
    FlutterViewController *flutterViewController = [[FlutterViewController alloc] init];
    
    KinSDKManager *kinClient = nil;
    kinClient = [[KinSDKManager alloc] initWithController:flutterViewController];
    
    [kinClient setup];
    
    [GeneratedPluginRegistrant registerWithRegistry:self];
    return [super application:application didFinishLaunchingWithOptions:launchOptions];
}

@end
