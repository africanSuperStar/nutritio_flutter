//
//  KinViewController.swift
//  Runner
//
//  Created by Cameron de Bruyn on 2019/09/06.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

import Foundation
import UIKit
import KinSDK
import KinUtil

//class Kin: UITableViewController {
//    
//    private var kinClient:  KinClient!
//    private var kinAccount: KinAccount!
//    private var watch:      BalanceWatch?
//    
//    private let linkBag = LinkBag()
//    
//    internal func setup(with kinClient: KinClient, kinAccount: KinAccount) {
//        self.kinClient  = kinClient
//        self.kinAccount = kinAccount
//    }
//    
//    init() {
//        
//        self.watch = try? self.kinAccount.watchBalance(0)
//        self.watch?.emitter.on(queue: .main, next: { [weak self] balance in
//            guard let this = self else { return }
//            
//            if let balanceCell = this.tableView.visibleCells.compactMap({ $0 as? BalanceTableViewCell }).first {
//                balanceCell.balance = balance
//            }
//        })
//        .add(to: self.linkBag)
//    }
//    
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = super.tableView(tableView, cellForRowAt: indexPath)
//        
//        if let kCell = cell as? KinClientCell {
//            kCell.kinClient = kinClient
//            kCell.kinAccount = kinAccount
//            kCell.kinClientCellDelegate = self
//        }
//        
//        return cell
//    }
//    
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 6
//    }
//}
//
//extension KinViewController: KinClientCellDelegate {
//    func revealKeyStore() {
//        guard let keyStoreViewController = storyboard?.instantiateViewController(withIdentifier: "KeyStoreViewController") as? KeyStoreViewController else {
//            return
//        }
//        
//        keyStoreViewController.view.tintColor = view.tintColor
//        keyStoreViewController.kinClient = kinClient
//        navigationController?.pushViewController(keyStoreViewController, animated: true)
//    }
//    
//    func startSendTransaction() {
//        guard let txViewController = storyboard?.instantiateViewController(withIdentifier: "SendTransactionViewController") as? SendTransactionViewController else {
//            return
//        }
//        
//        txViewController.view.tintColor = view.tintColor
//        txViewController.kinClient = kinClient
//        txViewController.kinAccount = kinAccount
//        navigationController?.pushViewController(txViewController, animated: true)
//    }
//    
//    func deleteAccountTapped() {
//        let alertController = UIAlertController(title: " Wallet?",
//                                                message: "Deleting a wallet will cause funds to be lost",
//                                                preferredStyle: .actionSheet)
//        let deleteAction = UIAlertAction(title: "OK", style: .destructive) { _ in
//            try? self.kinClient.deleteAccount(at: 0)
//            self.watch = nil
//            self.navigationController?.popViewController(animated: true)
//        }
//        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
//        
//        alertController.addAction(deleteAction)
//        alertController.addAction(cancelAction)
//        present(alertController, animated: true, completion: nil)
//    }
//    
//    func recentTransactionsTapped() {
//        guard let txViewController = storyboard?.instantiateViewController(withIdentifier: "RecentTxsTableViewController") as? RecentTxsTableViewController else {
//            return
//        }
//        
//        txViewController.kinAccount = kinAccount
//        txViewController.view.tintColor = view.tintColor
//        navigationController?.pushViewController(txViewController, animated: true)
//    }
//    
//    func getTestKin(cell: KinClientCell) {
//        guard let getKinCell = cell as? GetKinTableViewCell else {
//            return
//        }
//        
//        getKinCell.getKinButton.isEnabled = false
//        
//        print("Creating account.")
//        
//        self.createAccount()
//            .then(on: .main, { _ in
//                getKinCell.getKinButton.isEnabled = true
//            })
//        
//        try! kinAccount.watchCreation()
//            .finally({
//                print("I see a new account!")
//            })
//    }
//    
//    enum OnBoardingError: Error {
//        case invalidResponse
//        case errorResponse
//    }
//    
//    private func createAccount() -> Promise<Bool> {
//        let p = Promise(true)
//        let url = URL(string: "http://friendbot-testnet.kininfrastructure.com?addr=\(kinAccount.publicAddress)")!
//        
//        URLSession.shared.dataTask(with: url, completionHandler: { data, response, error in
//            guard
//                let data = data,
//                let jsonOpt = try? JSONSerialization.jsonObject(with: data, options: []),
//                let _ = jsonOpt as? [String: Any]
//                else {
//                    print("Unable to parse json for createAccount().")
//                    
//                    p.signal(OnBoardingError.invalidResponse)
//                    
//                    return
//            }
//            
//            p.signal(true)
//        }).resume()
//        
//        return p
//    }
//    
//    private func fund(amount: Kin) -> Promise<Bool> {
//        let p = Promise(true)
//        let url = URL(string: "http://friendbot-testnet.kininfrastructure.com/fund?addr=\(kinAccount.publicAddress)&amount=\(amount)")!
//        
//        URLSession.shared.dataTask(with: url, completionHandler: { data, response, error in
//            guard
//                let data = data,
//                let jsonOpt = try? JSONSerialization.jsonObject(with: data, options: []),
//                let json = jsonOpt as? [String: Any]
//                else {
//                    print("Unable to parse json for fund().")
//                    
//                    p.signal(OnBoardingError.invalidResponse)
//                    
//                    return
//            }
//            
//            guard let status = json["success"] as? Int, status != 0 else {
//                p.signal(OnBoardingError.errorResponse)
//                
//                return
//            }
//            
//            p.signal(true)
//        }).resume()
//        
//        return p
//    }
//    
//}
