//
//  KinSDK.swift
//  Runner
//
//  Created by Cameron de Bruyn on 2019/09/02.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

import Foundation
import UIKit
import KinSDK
import KinUtil

struct Provider: ServiceProvider {
    public let url:     URL
    public let network: Network
    
    init(url: URL, network: Network) {
        self.url     = url
        self.network = network
    }
}

@objc class KinSDKManager : NSObject {
    let production = false
    
    let channel:   FlutterMethodChannel
    
    var kinClient: KinClient?
    var provider:  Provider

    unowned let flutterViewController: FlutterViewController
    
    @objc init(controller: FlutterViewController) {
        self.flutterViewController = controller
 
        channel = FlutterMethodChannel(name: "za.org.nutritio/kin", binaryMessenger: flutterViewController as! FlutterBinaryMessenger)
        
        if production {
            provider = Provider(url: URL(string: "https://horizon.kininfrastructure.com")!, network: .mainNet)
        }
        else {
            provider = Provider(url: URL(string: "http://horizon-testnet.kininfrastructure.com")!, network: .testNet)
        }
        
        do {
            self.kinClient = try KinClient(provider: provider, appId: AppId(ACCOUNT_ID))
        }
        catch {
            let error = FlutterError(code: "UNAVAILABLE",
                                     message: "KinClient unavailable",
                                     details: nil)
            print(error)
        }
        
        super.init()
    }
    
    @objc func setup() {
        channel.setMethodCallHandler {
            (call, result) in
            
            switch call.method {
            case "createKinAccount":
                self.createKinAccount(with: self.kinClient!, production: self.production)
            
            case "retrieveAccountDetails":
                if let kinAccount = self.kinClient?.accounts.first {
//                    self.showKinViewController(with: self.kinClient!, kinAccount: kinAccount, production: self.production)
                }
            
            case "kinClientExists":
                let exists = self.kinClientExists(self.kinClient)
                result(exists)
            default:
                result(FlutterMethodNotImplemented)
                return
            }
        }
    }
    
    /**
     Initializes the Kin Client with the playground environment.
     */
    func createKinAccount(with kinClient: KinClient, production: Bool) {
        let testOrMainNet = production ? "Main" : "Test"
        
        let alertController = UIAlertController(title: "No \(testOrMainNet) Net Wallet Yet", message: "Let's create a new one, using the kinClient.createAccountIfNeeded() api.", preferredStyle: .alert)
        let createWalletAction = UIAlertAction(title: "Create a Wallet", style: .default) { _ in
            do {
                let kinAccount = try kinClient.addAccount()
//                self.showKinViewController(with: kinClient, kinAccount: kinAccount, production: production)
            } catch {
                let error = FlutterError(code: "kinaccount_not_created", message: "KinAccount couldn't be created: \(error)", details: nil)
                print(error)
            }
        }
        let importWalletAction = UIAlertAction(title: "Import a Wallet", style: .default) { _ in
            self.importKinAccount(with: kinClient, production: production)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
    
        alertController.addAction(createWalletAction)
        alertController.addAction(importWalletAction)
        alertController.addAction(cancelAction)
        self.flutterViewController.present(alertController, animated: true, completion: nil)
    }
    
    func importKinAccount(with kinClient: KinClient, production: Bool) {
        var removeObserver: (()->())?
        let passphraseTag = 1
        let importTag = 2
        
        let alertController = UIAlertController(title: "Import a Wallet", message: nil, preferredStyle: .alert)
        let importWalletAction = UIAlertAction(title: "Import", style: .default) { _ in
            if let removeObserver = removeObserver {
                removeObserver()
            }
            
            guard let importTextField = alertController.textFields?.first(where: { $0.tag == importTag }),
                let jsonString = importTextField.text,
                let passphraseTextField = alertController.textFields?.first(where: { $0.tag == passphraseTag })
                else {
                    print("Invalid import string")
                    return
            }
            
            let passphrase = passphraseTextField.text ?? ""
            
            do {
                let kinAccount = try kinClient.importAccount(jsonString, passphrase: passphrase)
//                self.showKinViewController(with: kinClient, kinAccount: kinAccount, production: production)
            }
            catch {
                print("KinAccount couldn't be imported: \(error)")
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            if let removeObserver = removeObserver {
                removeObserver()
            }
        }
        
        importWalletAction.isEnabled = false
        
        alertController.addTextField { passphraseTextField in
            passphraseTextField.tag = passphraseTag
            passphraseTextField.placeholder = "Passphrase"
        }
        alertController.addTextField { importTextField in
            let observer = NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: importTextField, queue: nil, using: { _ in
                guard let importString = importTextField.text?.trimmingCharacters(in: .whitespaces) else {
                    return
                }
                
                importWalletAction.isEnabled = !importString.isEmpty
            })
            
            removeObserver = {
                NotificationCenter.default.removeObserver(observer)
            }
            
            importTextField.tag = importTag
            importTextField.placeholder = "JSON String"
        }
        alertController.addAction(importWalletAction)
        alertController.addAction(cancelAction)
        self.flutterViewController.present(alertController, animated: true, completion: nil)
    }
    
    func kinClientExists(_ client: KinClient?) -> Bool {
        if client != nil {
            return true
        }
        return false
    }
    
}
