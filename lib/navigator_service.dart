import 'package:flutter/material.dart';
import 'package:nutritio/utils/colors.dart';

class NavigatorProvider extends InheritedWidget {
  final NavigatorService service = NavigatorService();

  NavigatorProvider({Widget child}) : super(child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  static NavigatorProvider of(BuildContext context) =>
      context.inheritFromWidgetOfExactType(NavigatorProvider);
}

class NavigatorService {
  Widget currentPage = Container(
    color: DaintyColors.background,
  );

  var scrollController = ScrollController();

  final GlobalKey<NavigatorState> navigatorKey = GlobalKey();
}
