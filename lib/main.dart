import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';

import 'core/onboarding/onboarding_widget.dart';
import 'features/auth/presentation/bloc/bloc.dart';
import 'features/auth/presentation/widgets/auth_widget.dart';
import 'features/home/presentation/pages/home_widget.dart';
import 'navigator_service.dart';
import 'utils/routes.dart';
import 'utils/theme.dart';

///
/// Main Flutter Application
///
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  BlocSupervisor.delegate = await HydratedBlocDelegate.build();

  runApp(
    NavigatorProvider(child: NutritioApp()),  
  );
}

class NutritioApp extends StatefulWidget {
  @override
  State<NutritioApp> createState() => _NutritioAppState();
}

class _NutritioAppState extends State<NutritioApp> {
  ThemeData daintyTheme;

  @override
  void initState() {
    daintyTheme = buildDaintyTheme();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final NavigatorService _service = NavigatorProvider.of(context).service;

    return MaterialApp(
      navigatorKey: _service.navigatorKey,
      theme: daintyTheme,
      home: NutritioAppChild(),
      routes: routes
    );
  }
}

class NutritioAppChild extends StatelessWidget {
  
  const NutritioAppChild({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        builder: (context) => AuthBloc(),
        child: Container( 
          alignment: Alignment.center,
          child: BlocBuilder<AuthBloc, AuthState>(
            builder: (BuildContext context, AuthState state) {
              if (state is Uninitialized) {
                return buildInitialOnboardingScreen();
              }
              if (state is Unauthenticated) {
                return buildAuthenticationScreen();
              }
              if (state is Authenticated) {
                return buildHomeScreen();
              }
              return buildLoading();
            },
          ),
        ),
      ),
    );
  }

  Widget buildInitialOnboardingScreen() {
    return OnboardingWidget();
  }

  Widget buildAuthenticationScreen() {
    return AuthenticationWidget();
  }

  Widget buildHomeScreen() {
    return HomeWidget();
  }

  Widget buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }
}
