import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../navigator_service.dart';
import '../../../calendar/presentation/widgets/calendar_popup_view.dart';
import '../../../food_diary/presentation/widgets/glass_view.dart';
import '../../../food_diary/presentation/widgets/water_view.dart';
import '../../../home/presentation/widgets/custom_appbar.dart';
import '../bloc/bloc.dart';
import '../widgets/timeline.dart';
import '../widgets/timeline_card_view.dart';
import '../widgets/timeline_model.dart';

class WaterDiaryScreen extends StatefulWidget {
  final AnimationController animationController;

  WaterDiaryScreen({
    Key key,
    this.animationController,
  }) : super(key: key);

  @override
  _WaterDiaryScreenState createState() => _WaterDiaryScreenState();
}

class _WaterDiaryScreenState extends State<WaterDiaryScreen> {
  WaterDiaryBloc _waterDiaryBloc;

  DateTime startDate = DateTime.now().subtract(Duration(days: 5));
  DateTime endDate = DateTime.now();

  List<Widget> listViews = List<Widget>();

  @override
  void initState() {
    _waterDiaryBloc = BlocProvider.of<WaterDiaryBloc>(context);

    addAllListData();

    super.initState();
  }

  @override
  void dispose() {
    _waterDiaryBloc.close();
    super.dispose();
  }

  void addAllListData() {
    var count = 9;
    listViews.add(
      WaterView(
        mainScreenAnimation: Tween(begin: 0.0, end: 1.0).animate(
            CurvedAnimation(
                parent: widget.animationController,
                curve: Interval((1 / count) * 7, 1.0,
                    curve: Curves.fastOutSlowIn))),
        mainScreenAnimationController: widget.animationController,
      ),
    );
    listViews.add(
      GlassView(
        animation: Tween(begin: 0.0, end: 1.0).animate(
          CurvedAnimation(
            parent: widget.animationController,
            curve: Interval((1 / count) * 8, 1.0, curve: Curves.fastOutSlowIn),
          ),
        ),
        animationController: widget.animationController,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (!mounted) {
      CustomAppBar.title = "Liquid Diary";
    }

    void showCalendarDialog() {
      widget.animationController.reverse().then((data) {
        if (!mounted) return;
        showDialog(
          context: context,
          builder: (BuildContext context) => CalendarPopupView(
            barrierDismissible: true,
            minimumDate: DateTime.now().subtract(Duration(days: 15)),
            initialEndDate: endDate,
            initialStartDate: startDate,
            onApplyClick: (DateTime startData, DateTime endData) {
              setState(() {
                if (startData != null && endData != null) {
                  startDate = startData;
                  endDate = endData;
                }
                _waterDiaryBloc.add(DeactivateCalendarDialog());
              });
            },
            onCancelClick: () {},
          ),
        );
      });
    }

    return BlocBuilder(
      bloc: _waterDiaryBloc,
      builder: (BuildContext context, WaterDiaryState state) {
        if (state is WaterDiaryLoading) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else if (state is CalendarDialogActive) {
          _waterDiaryBloc.add(LoadCalendarDialog());
          showCalendarDialog();
        }
        return buildWaterDiaryPage();
      },
    );
  }

  Widget buildWaterDiaryPage() {
    return CustomScrollView(
      semanticChildCount: 4,
      slivers: <Widget>[
        SliverAppBar(
          automaticallyImplyLeading: false,
          expandedHeight: 500,
          flexibleSpace: FlexibleSpaceBar(
            background: getMainListView(),
          ),
        ),
        SliverList(
          delegate: SliverChildListDelegate([
            getTimelineListView(),
          ]),
        ),
      ],
    );
  }

  Future<bool> getData() async {
    await Future.delayed(const Duration(milliseconds: 50));
    return true;
  }

  Widget getMainListView() {
    return BlocBuilder(
      bloc: _waterDiaryBloc,
      builder: (BuildContext context, WaterDiaryState state) {
        if (state is WaterDiaryLoading || state is CalendarDialogLoading) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        return ListView.builder(
          controller: NavigatorProvider.of(context).service.scrollController,
          padding: EdgeInsets.only(
            top: AppBar().preferredSize.height +
                MediaQuery.of(context).padding.top +
                24,
            bottom: 62 + MediaQuery.of(context).padding.bottom,
          ),
          itemCount: listViews.length,
          scrollDirection: Axis.vertical,
          itemBuilder: (context, index) {
            widget.animationController.forward();
            return listViews[index];
          },
        );
      },
    );
  }

  Widget getTimelineListView() {
    return BlocBuilder(
      bloc: _waterDiaryBloc,
      builder: (BuildContext context, WaterDiaryState state) {
        if (state is WaterDiaryLoading || state is CalendarDialogLoading) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else if (state is WaterDiaryLoaded) {
          return timelineModel(TimelinePosition.Left, state.waterEntry.length);
        } else {
          print(state);
          return Container();
        }
      },
    );
  }

  timelineModel(TimelinePosition position, int length) => Timeline.builder(
      itemBuilder: centerTimelineBuilder,
      itemCount: length,
      physics: position == TimelinePosition.Left
          ? ClampingScrollPhysics()
          : BouncingScrollPhysics(),
      position: position);

  TimelineModel centerTimelineBuilder(BuildContext context, int i) {
    return TimelineModel(
      timelineCard(i),
      position:
          i % 2 == 0 ? TimelineItemPosition.right : TimelineItemPosition.left,
      isFirst: i == 0,
    );
  }

  Widget timelineCard(int i) {
    return BlocBuilder(
      bloc: _waterDiaryBloc,
      builder: (BuildContext context, WaterDiaryState state) {
        if (state is WaterDiaryLoaded) {
          return TimelineCardView(
            index: i,
            animationController: widget.animationController,
            animation: Tween(begin: 0.0, end: 1.0).animate(
              CurvedAnimation(
                parent: widget.animationController,
                curve: Interval((1 / i) * 5, 1.0, curve: Curves.fastOutSlowIn),
              ),
            ),
          );
        } else {
          return Container();
        }
      },
    );
  }
}
