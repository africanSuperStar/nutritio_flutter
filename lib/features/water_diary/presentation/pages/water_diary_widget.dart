import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/bloc.dart';
import 'water_diary_screen.dart';

class WaterDiaryWidget extends StatefulWidget {
  @override
  _WaterDiaryWidgetState createState() => _WaterDiaryWidgetState();
}

class _WaterDiaryWidgetState extends State<WaterDiaryWidget>
    with TickerProviderStateMixin {
  AnimationController _animationController;

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _animationController = AnimationController(
      duration: Duration(milliseconds: 1000),
      vsync: this,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider<WaterDiaryBloc>(
        builder: (context) => WaterDiaryBloc(),
        child: WaterDiaryScreen(
          animationController: _animationController,
        ),
      ),
    );
  }
}
