import 'dart:async';
import 'dart:math';

import 'package:bloc/bloc.dart';

import './bloc.dart';
import '../../../../data/dao/water_dao.dart';
import '../../../../models/meals/water.dart';

class WaterDiaryBloc extends Bloc<WaterDiaryEvent, WaterDiaryState> {
  WaterDao _waterDao = WaterDao();

  @override
  WaterDiaryState get initialState => CalendarDialogActive();

  @override
  Stream<WaterDiaryState> mapEventToState(
    WaterDiaryEvent event,
  ) async* {
    if (event is ActivateCalendarDialog) {
      yield CalendarDialogActive();
      yield* _reloadWaterEntries();
    } else if (event is DeactivateCalendarDialog) {
      yield CalendarDialogInactive();
      yield* _reloadWaterEntries();
    } else if (event is LoadWaterEntries) {
      // Indicating that water entries are being loaded - display progress indicator.
      yield WaterDiaryLoading();
      yield* _reloadWaterEntries();
    } else if (event is LoadCalendarDialog) {
      // Indicating that water entries are being loaded - display progress indicator.
      yield CalendarDialogLoading();
      yield* _reloadWaterEntries();
    } else if (event is AddRandomEntry) {
      // Loading indicator shouldn't be displayed while adding/updating/deleting
      // a single Fruit from the database - we aren't yielding FruitsLoading().
      await _waterDao.insert(RandomWaterGenerator.getRandomWaterEntries());
      yield* _reloadWaterEntries();
    } else if (event is UpdateWithRandomEntry) {
      final newFruit = RandomWaterGenerator.getRandomWaterEntries();
      // Keeping the ID of the Fruit the same
      newFruit.id = event.updatedWaterEntry.id;
      await _waterDao.update(newFruit);
      yield* _reloadWaterEntries();
    } else if (event is DeleteWaterEntry) {
      await _waterDao.delete(event.water);
      yield* _reloadWaterEntries();
    }
  }

  Stream<WaterDiaryState> _reloadWaterEntries() async* {
    // final waterDiar = await _waterDao.getAllSortedByName();
    final _waterEntries = [
      Water(
        dailyGoal: 2,
        currentLevel: 1,
        lastDrink: "2 hours ago",
        timeToRefill: false,
      ),
      Water(
        dailyGoal: 2,
        currentLevel: 1,
        lastDrink: "2 hours ago",
        timeToRefill: false,
      ),
      Water(
        dailyGoal: 2,
        currentLevel: 1,
        lastDrink: "2 hours ago",
        timeToRefill: false,
      ),
      Water(
        dailyGoal: 2,
        currentLevel: 1,
        lastDrink: "2 hours ago",
        timeToRefill: false,
      )
    ];
    // Yielding a state bundled with the Fruits from the database.
    yield WaterDiaryLoaded(_waterEntries);
  }
}

class RandomWaterGenerator {
  static final _waterEntries = [
    Water(
      dailyGoal: 2,
      currentLevel: 1,
      lastDrink: "2 hours ago",
      timeToRefill: false,
    ),
    Water(
      dailyGoal: 2,
      currentLevel: 1,
      lastDrink: "2 hours ago",
      timeToRefill: false,
    ),
    Water(
      dailyGoal: 2,
      currentLevel: 1,
      lastDrink: "2 hours ago",
      timeToRefill: false,
    ),
    Water(
      dailyGoal: 2,
      currentLevel: 1,
      lastDrink: "2 hours ago",
      timeToRefill: false,
    )
  ];

  static Water getRandomWaterEntries() {
    return _waterEntries[Random().nextInt(_waterEntries.length)];
  }
}
