import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../../models/meals/water.dart';

@immutable
abstract class WaterDiaryEvent extends Equatable {
  WaterDiaryEvent([List props = const <dynamic>[]]) : super();
}

class ActivateCalendarDialog extends WaterDiaryEvent {
  @override
  String toString() => 'ActivateCalendarDialog';

  @override
  List<Object> get props => null;
}

class DeactivateCalendarDialog extends WaterDiaryEvent {
  @override
  String toString() => 'DeactivateCalendarDialog';

  @override
  List<Object> get props => null;
}

class LoadWaterEntries extends WaterDiaryEvent {
  @override
  List<Object> get props => null;
}

class LoadCalendarDialog extends WaterDiaryEvent {
  @override
  List<Object> get props => null;
}

class AddRandomEntry extends WaterDiaryEvent {
  @override
  List<Object> get props => null;
}

class UpdateWithRandomEntry extends WaterDiaryEvent {
  final Water updatedWaterEntry;

  UpdateWithRandomEntry(this.updatedWaterEntry) : super([updatedWaterEntry]);

  @override
  List<Object> get props => [this.updatedWaterEntry];
}

class DeleteWaterEntry extends WaterDiaryEvent {
  final Water water;

  DeleteWaterEntry(this.water) : super([water]);

  @override
  List<Object> get props => [this.water];
}
