import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../../models/meals/water.dart';

@immutable
abstract class WaterDiaryState extends Equatable {
  WaterDiaryState([List props = const <dynamic>[]]) : super();
}

class InitialWaterDiaryState extends WaterDiaryState {
  @override
  List<Object> get props => [];
}

class WaterDiaryLoading extends WaterDiaryState {
  @override
  List<Object> get props => null;
}

class CalendarDialogLoading extends WaterDiaryState {
  @override
  List<Object> get props => null;
}

class WaterDiaryLoaded extends WaterDiaryState {
  final List<Water> waterEntry;

  WaterDiaryLoaded(this.waterEntry) : super([waterEntry]);

  @override
  List<Object> get props => [this.waterEntry];
}

class CalendarDialogActive extends WaterDiaryState {
  static final route = "/calendar_modal";

  @override
  List<Object> get props => null;
}

class CalendarDialogInactive extends WaterDiaryState {
  @override
  String toString() => 'Calendar Inactive';

  @override
  List<Object> get props => null;
}
