import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class CalendarState extends Equatable {
  CalendarState([List props = const <dynamic>[]]) : super();
}

class InitialCalendarState extends CalendarState {
  @override
  List<Object> get props => [];
}
