import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class CalendarEvent extends Equatable {
  CalendarEvent([List props = const <dynamic>[]]) : super();
}
