import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class SettingsState extends Equatable {
  SettingsState([List props = const <dynamic>[]]) : super();
}

class InitialSettingsState extends SettingsState {
  @override
  List<Object> get props => null;
}
