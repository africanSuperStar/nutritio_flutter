import 'package:flutter/material.dart';
import 'package:nutritio/models/paypal_oauth/model.dart';
import 'package:nutritio/models/paypal_oauth/paypal_data_source.dart';

class SettingsScreen extends StatefulWidget {
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  final _dataSource = PayPalDataSource();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          automaticallyImplyLeading: true,
          title: Text('Settings'),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => _createProduct(_dataSource),
          )),
      body: Container(),
    );
  }

  void _createProduct(PayPalDataSource dataSource) async {
    Future<PayPalTokenResult> token;

    // final paypalProduct = PayPalProductAbstractBuilder()
    //   ..name = 'Nutritio Health Product'
    //   ..description = 'Blah Blah Blah'
    //   ..type = PayPalType[PayPalTypeEnum.SERVICE]
    //   ..category = PayPalCategory[PayPalCategoryEnum.HEALTH_AND_NUTRITION]
    //   ..imageURL = ''
    //   ..homeURL = '';

    token = dataSource.receiveAccessToken();
    dataSource.listProducts(token: await token);
  }
}
