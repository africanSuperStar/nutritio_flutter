import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';

import '../../../../core/error/failures.dart';
import '../entities/user.dart';

abstract class IUserRepository {
  Future<Either<Failure, FirebaseUser>> signInWithGoogle();

  Future<Either<Failure, FirebaseUser>> signInWithFacebook();

  Future<Either<Failure, FirebaseUser>> signInWithTwitter();

  Future<Either<Failure, FirebaseUser>> signInWithCredentials(String email, String password);

  Future<Either<Failure, void>> signUp({String email, String password});

  Future<Either<Failure, void>> signOut();

  Future<Either<Failure, bool>> isSignedIn();

  Future<Either<Failure, User>> getUser();
}
