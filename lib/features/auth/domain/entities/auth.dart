import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

import 'user.dart';

part 'auth.g.dart';

@JsonSerializable()
class Auth extends Equatable {
  final User user;

  Auth({
    @required this.user
  })  : super();

  factory Auth.fromJson(Map<String, dynamic> json) => _$AuthFromJson(json);
  Map<String, dynamic> toJson() => _$AuthToJson(this);

  @override
  List<Object> get props => [user];
}

@JsonSerializable()
class AuthInitialized extends Equatable {
  final bool isInitialized;

  AuthInitialized({
    @required this.isInitialized
  }) : super();

  factory AuthInitialized.fromJson(Map<String, dynamic> json) => _$AuthInitializedFromJson(json);
  Map<String, dynamic> toJson() => _$AuthInitializedToJson(this);

  @override
  List<Object> get props => [isInitialized];
}
