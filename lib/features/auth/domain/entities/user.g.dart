// GENERATED CODE - DO NOT MODIFY BY HAND

part of authentication;

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    userStatus: json['userStatus'] == null
        ? null
        : UserStatus.fromJson(json['userStatus'] as Map<String, dynamic>),
    displayName: json['displayName'] as String,
    email: json['email'] as String,
    token: json['token'] as String,
    metaData: json['metaData'] == null
        ? null
        : UserMetadata.fromJson(json['metaData'] as Map<String, dynamic>),
    phoneNumber: json['phoneNumber'] as String,
    providerId: json['providerId'] as String,
    uid: json['uid'] as String,
    isAnonymous: json['isAnonymous'] as bool,
  );
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'userStatus': instance.userStatus,
      'displayName': instance.displayName,
      'email': instance.email,
      'token': instance.token,
      'metaData': instance.metaData,
      'phoneNumber': instance.phoneNumber,
      'providerId': instance.providerId,
      'uid': instance.uid,
      'isAnonymous': instance.isAnonymous,
    };
