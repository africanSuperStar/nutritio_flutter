library user_metadata;

import 'package:built_value/built_value.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user_metadata.g.dart';

@JsonSerializable()
class UserMetadata extends Equatable {
  @nullable
  final int creationTimestamp;

  @nullable
  final int lastLoginTimestamp;

  UserMetadata({
    this.creationTimestamp,
    this.lastLoginTimestamp  
  });

  factory UserMetadata.fromJson(Map<String, dynamic> json) => _$UserMetadataFromJson(json);
  Map<String, dynamic> toJson() => _$UserMetadataToJson(this);

  @override
  List<Object> get props => [creationTimestamp, lastLoginTimestamp];
}
