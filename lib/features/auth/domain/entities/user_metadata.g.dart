// GENERATED CODE - DO NOT MODIFY BY HAND

part of user_metadata;

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserMetadata _$UserMetadataFromJson(Map<String, dynamic> json) {
  return UserMetadata(
    creationTimestamp: json['creationTimestamp'] as int,
    lastLoginTimestamp: json['lastLoginTimestamp'] as int,
  );
}

Map<String, dynamic> _$UserMetadataToJson(UserMetadata instance) =>
    <String, dynamic>{
      'creationTimestamp': instance.creationTimestamp,
      'lastLoginTimestamp': instance.lastLoginTimestamp,
    };
