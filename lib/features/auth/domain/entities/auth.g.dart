// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Auth _$AuthFromJson(Map<String, dynamic> json) {
  return Auth(
    user: json['user'] == null
        ? null
        : User.fromJson(json['user'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$AuthToJson(Auth instance) => <String, dynamic>{
      'user': instance.user,
    };

AuthInitialized _$AuthInitializedFromJson(Map<String, dynamic> json) {
  return AuthInitialized(
    isInitialized: json['isInitialized'] as bool,
  );
}

Map<String, dynamic> _$AuthInitializedToJson(AuthInitialized instance) =>
    <String, dynamic>{
      'isInitialized': instance.isInitialized,
    };
