library authentication;

import 'package:built_value/built_value.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:nutritio/features/auth/domain/entities/user_status.dart';

import 'user_metadata.dart';

part 'user.g.dart';

@JsonSerializable()
class User extends Equatable {

  final UserStatus userStatus;

  final String displayName;
  final String email;
  final String token;
  final UserMetadata metaData;

  @nullable
  final String phoneNumber;

  @nullable
  final String providerId;

  @nullable
  final String uid;

  @nullable
  final bool isAnonymous;

  User({
    @required this.userStatus,
    @required this.displayName,
    @required this.email,
    @required this.token,
    @required this.metaData,
    this.phoneNumber,
    this.providerId,
    this.uid,
    this.isAnonymous
  }) : super();

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);

  @override
  List<Object> get props => [
    userStatus,
    displayName,
    email,
    token,
    metaData,
    phoneNumber,
    providerId,
    uid,
    isAnonymous
  ];
}
