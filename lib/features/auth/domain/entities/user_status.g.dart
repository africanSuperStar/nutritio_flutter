// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_status.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserStatus _$UserStatusFromJson(Map<String, dynamic> json) {
  return UserStatus(
    isSignedIn: json['isSignedIn'] as bool,
    isInitialized: json['isInitialized'] as bool,
  );
}

Map<String, dynamic> _$UserStatusToJson(UserStatus instance) =>
    <String, dynamic>{
      'isSignedIn': instance.isSignedIn,
      'isInitialized': instance.isInitialized,
    };
