import 'package:flutter/material.dart';
import 'package:built_value/built_value.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user_status.g.dart';

@JsonSerializable()
class UserStatus {
  @nullable
  bool isSignedIn;
  bool isInitialized;

  UserStatus({
    @required this.isSignedIn,
    @required this.isInitialized
  });

  factory UserStatus.fromJson(Map<String, dynamic> json) => _$UserStatusFromJson(json);
  Map<String, dynamic> toJson() => _$UserStatusToJson(this);
}
