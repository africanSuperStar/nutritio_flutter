import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class RegistrationEvent extends Equatable {
  RegistrationEvent([List props = const <dynamic>[]]) : super();
}

class NewRegistrationPressed extends RegistrationEvent {
  @override
  String toString() => 'New Registration';

  @override
  List<Object> get props => null;
}

class UserForgotPassword extends RegistrationEvent {
  @override
  String toString() => 'User forgot their password';

  @override
  List<Object> get props => null;
}
