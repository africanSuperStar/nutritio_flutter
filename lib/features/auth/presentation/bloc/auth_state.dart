import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../domain/entities/user.dart';

@immutable
abstract class AuthState extends Equatable {
  AuthState([List props = const <dynamic>[]]) : super();
}

class Initialized extends AuthState {
  @override
  String toString() => 'User Initialized';

  @override
  List<Object> get props => null;
}

class Uninitialized extends AuthState {
  @override
  String toString() => 'User Uninitialized';

  @override
  List<Object> get props => null;
}

class AuthLoading extends AuthState {
  @override
  String toString() => 'Authentication Loading';

  @override
  List<Object> get props => null;
}

class AnalyticsDismissed extends AuthState {
  @override
  String toString() => 'Analytics Dismissed';

  @override
  List<Object> get props => null;
}

class AnalyticsAccepted extends AuthState {
  @override
  String toString() => 'Analytics Accepted';

  @override
  List<Object> get props => null;
}

class Authenticated extends AuthState {
  final User user;

  Authenticated(this.user) : super([user]);

  @override
  String toString() => 'Authenticated { user: $user }';

  @override
  List<Object> get props => [this.user];
}

class Unauthenticated extends AuthState {
  final User user;

  Unauthenticated(this.user) : super([user]);

  @override
  String toString() => 'Unauthenticated';

  @override
  List<Object> get props => [this.user];
}
