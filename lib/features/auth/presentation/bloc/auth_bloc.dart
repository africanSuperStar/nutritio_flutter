import 'dart:async';

import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:nutritio/core/error/failures.dart';
import 'package:nutritio/features/auth/data/repositories/user_repository.dart';
import 'package:nutritio/features/auth/domain/entities/user_status.dart';

import '../../domain/entities/user.dart';
import 'auth_event.dart';
import 'auth_state.dart';

const String SERVER_FAILURE_MESSAGE = 'Failure to connect to Firebase';

class AuthBloc extends HydratedBloc<AuthEvent, AuthState> {
  final BuildContext context;

  AuthBloc({this.context});

  final UserRepository userRepository = UserRepository();

  @override
  AuthState get initialState {
    return super.initialState ?? Uninitialized();
  }

  @override
  AuthState fromJson(Map<String, dynamic> json) {
    try {
      final user = User.fromJson(json);
      if (user.userStatus.isSignedIn) {
        return Authenticated(user);
      }
      return Unauthenticated(user);
    } catch (e) {
      return null;
    }
  }

  @override
  Map<String, dynamic> toJson(AuthState state) {
    if (state is Authenticated) {
      return state.user.toJson();
    } else if (state is Unauthenticated) {
      return state.user.toJson();
    }
    return null;
  }

  @override
  Stream<AuthState> mapEventToState(
    AuthEvent event,
  ) async* {
    if (event is LoggingIn) {
      yield* _mapLoggedInToState();
    } else if (event is LoggingOut) {
      yield* _mapLoggedOutToState();
    } else if (event is GettingAnalyticsPermission) {
      if (event.userPermission == true) {
        yield AnalyticsAccepted();
      } else {
        yield AnalyticsDismissed();
      }
    } else if (event is NavigateToAuthScreen) {
      User user = User(
        displayName: null,
        email: null,
        userStatus: UserStatus(isInitialized: true, isSignedIn: null),
        metaData: null,
        token: null,
      );
      yield Unauthenticated(user);
    }
  }

  Stream<AuthState> _mapLoggedInToState() async* {
    
    Either<Failure, User> failureOrUser = await userRepository.getUser();
    
    if(failureOrUser != null) {
      failureOrUser.fold(
        (failure) => _mapFailureToMessage(failure),
        (user) async* {
          if (user == null) {
            yield Unauthenticated(user);
            return;
          }
          user.userStatus.isInitialized = true;
          yield Initialized();
          user.userStatus.isSignedIn = true;
          yield Authenticated(user);
        }
      );
    }
  }

  Stream<AuthState> _mapLoggedOutToState() async* {
    
    Either<Failure, User> failureOrUser = await userRepository.getUser();
    
    if(failureOrUser != null) {
      failureOrUser.fold(
        (failure) => _mapFailureToMessage(failure),
        (user) async* {
          if (user != null) {
            user.userStatus.isSignedIn = false;
            yield Unauthenticated(user);
            userRepository.signOut();
          }
        }
      );
    }
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      default:
        return 'Unexpected error';
    }
  }
}
