import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class AuthEvent extends Equatable {
  AuthEvent([List props = const <dynamic>[]]) : super();
}

class GettingAnalyticsPermission extends AuthEvent {
  final bool userPermission;

  GettingAnalyticsPermission(this.userPermission) : super([userPermission]);

  @override
  String toString() => 'GetAnalyticsPermission';

  @override
  List<Object> get props => [this.userPermission];
}

class LoggingIn extends AuthEvent {
  @override
  String toString() => 'LoggedIn';

  @override
  List<Object> get props => null;
}

class LoggingOut extends AuthEvent {
  @override
  String toString() => 'LoggedOut';

  @override
  List<Object> get props => null;
}

class NavigateToAuthScreen extends AuthEvent {
  @override
  String toString() => 'NavigateToAuthScreen';

  @override
  List<Object> get props => null;
}
