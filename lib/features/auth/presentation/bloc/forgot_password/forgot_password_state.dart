import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ForgotPasswordState extends Equatable {
  
  ForgotPasswordState([List props = const <dynamic>[]]) : super();
}

class InitialForgotPasswordState extends ForgotPasswordState {
  
  @override
  List<Object> get props => null;
}
