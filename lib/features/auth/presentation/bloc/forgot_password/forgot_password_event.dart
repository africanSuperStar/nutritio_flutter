import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ForgotPasswordEvent extends Equatable {

  ForgotPasswordEvent([List props = const <dynamic>[]]) : super();
}
