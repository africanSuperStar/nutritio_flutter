import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/bloc.dart';
import '../../bloc/login/bloc.dart';
import 'login_form.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: null,
      body: BlocProvider<LoginBloc>(
        builder: (context) => LoginBloc(userRepository: BlocProvider.of<AuthBloc>(context).userRepository),
        child: LoginForm(),
      ),
    );
  }
}
