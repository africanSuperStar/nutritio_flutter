import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simple_animations/simple_animations.dart';

import '../../../../../navigator_service.dart';
import '../../bloc/bloc.dart';
import '../../bloc/login/bloc.dart';
import '../auth_buttons/facebook_login_button.dart';
import '../auth_buttons/google_login_button.dart';
import '../auth_buttons/twitter_login_button.dart';
import 'login_button.dart';

class LoginForm extends StatefulWidget {
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  LoginBloc _loginBloc;

  bool get isPopulated => _emailController.text.isNotEmpty && _passwordController.text.isNotEmpty;

  bool isLoginButtonEnabled(LoginState state) {
    return state.isFormValid && isPopulated && !state.isSubmitting;
  }

  @override
  void initState() {
    super.initState();
    _loginBloc = BlocProvider.of<LoginBloc>(context);
    _emailController.addListener(_onEmailChanged);
    _passwordController.addListener(_onPasswordChanged);
  }

  @override
  Widget build(BuildContext context) {
    NavigatorService _service = NavigatorProvider.of(context).service;

    final tween = MultiTrackTween([
      Track("top-color").add(Duration(seconds: 3), ColorTween(begin: Color(0xff45B39D), end: Color(0xff1D8348))),
      Track("bottom-color").add(Duration(seconds: 3), ColorTween(begin: Color(0xffA83279), end: Colors.blue.shade600))
    ]);

    return BlocListener(
      bloc: _loginBloc,
      listener: (context, state) {
        if (state.isFailure) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text('Login Failure'), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
        if (state.isSubmitting) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Logging In...'),
                    CircularProgressIndicator(),
                  ],
                ),
              ),
            );
        }
        if (state.isSuccess) {
          BlocProvider.of<AuthBloc>(context).add(LoggingIn());
          _service.navigatorKey.currentState.pushReplacementNamed('/home');
        }
      },
      child: BlocBuilder(
        bloc: _loginBloc,
        builder: (context, state) {
          return ControlledAnimation(
            playback: Playback.MIRROR,
            tween: tween,
            duration: tween.duration,
            builder: (context, animation) {
              return Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      animation["top-color"],
                      animation["bottom-color"],
                    ],
                  ),
                ),
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                      child: Image.asset('assets/images/logos/nutrit.io2.png'),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: TextFormField(
                        controller: _emailController,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Email',
                          labelStyle: TextStyle(color: Colors.white),
                        ),
                        autovalidate: true,
                        autocorrect: false,
                        validator: (_) {
                          return !state.isEmailValid ? 'Invalid Email' : null;
                        },
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: TextFormField(
                        controller: _passwordController,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Password',
                          labelStyle: TextStyle(color: Colors.white),
                        ),
                        obscureText: true,
                        autovalidate: true,
                        autocorrect: false,
                        validator: (_) {
                          return !state.isPasswordValid ? 'Invalid Password' : null;
                        },
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        GestureDetector(
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: 15),
                            child: Text("Forgot password?"),
                          ),
                          onTap: () => _service.navigatorKey.currentState.pushReplacementNamed('/registration'),
                        ),
                        GestureDetector(
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: 15),
                            child: Text("Register"),
                          ),
                          onTap: () =>
                              () => _service.navigatorKey.currentState.pushReplacementNamed('/forgot-password'),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 20.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            width: 300,
                            child: LoginButton(
                              onPressed: isLoginButtonEnabled(state) ? _onFormSubmitted : null,
                            ),
                          ),
                          SizedBox(
                            width: 300,
                            child: GoogleLoginButton(),
                          ),
                          SizedBox(
                            width: 300,
                            child: FacebookLoginButton(),
                          ),
                          SizedBox(
                            width: 300,
                            child: TwitterLoginButton(),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              );
            },
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  void _onEmailChanged() {
    _loginBloc.add(
      EmailChanged(email: _emailController.text),
    );
  }

  void _onPasswordChanged() {
    _loginBloc.add(
      PasswordChanged(password: _passwordController.text),
    );
  }

  void _onFormSubmitted() {
    _loginBloc.add(
      LoginWithCredentialsPressed(
        email: _emailController.text,
        password: _passwordController.text,
      ),
    );
  }
}
