import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/auth_bloc.dart';
import '../bloc/bloc.dart';
import 'login/login_screen.dart';

class AuthScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  AuthBloc _authenticationBloc;

  @override
  void initState() {
    super.initState();
    _authenticationBloc = BlocProvider.of<AuthBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    void _showDialog() {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Nutritio Diagnostic Analytics"),
            content: Text("Do you allow Nutritio to collect anonymous usage and crash diagnostic analytics?"),
            actions: <Widget>[
              FlatButton(
                child: Text("Accept"),
                onPressed: () {
                  _authenticationBloc.add(GettingAnalyticsPermission(true));
                  Navigator.of(context).pop();
                },
              ),
              FlatButton(
                child: Text("Close"),
                onPressed: () {
                  _authenticationBloc.add(GettingAnalyticsPermission(false));
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }

    return BlocListener(
      bloc: _authenticationBloc,
      listener: (context, state) {
        if (state is Uninitialized) {
          _showDialog();
        }
      },
      child: BlocBuilder(
        bloc: _authenticationBloc,
        builder: (context, state) {
          return LoginScreen();
        },
      ),
    );
  }
}
