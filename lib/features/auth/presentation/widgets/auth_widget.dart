import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/bloc.dart';
import 'auth_screen.dart';

class AuthenticationWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: null,
      body: BlocProvider<AuthBloc>(
        builder: (context) => AuthBloc(),
        child: AuthScreen(),
      ),
    );
  }
}
