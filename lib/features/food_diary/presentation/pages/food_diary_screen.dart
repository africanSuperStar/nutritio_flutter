import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../navigator_service.dart';
import '../../../../utils/colors.dart';
import '../../../calorie_tracker/presentation/pages/calorie_tracker.dart';
import '../../../home/presentation/bloc/bloc.dart';
import '../../../home/presentation/bloc/home_bloc.dart';
import '../../../home/presentation/widgets/custom_appbar.dart';
import '../../../water_diary/presentation/pages/water_diary_widget.dart';
import '../widgets/body_measurement_view.dart';
import '../widgets/glass_view.dart';
import '../widgets/meals_list_view.dart';
import '../widgets/prescribed_diet_view.dart';
import '../widgets/title_view.dart';
import '../widgets/water_view.dart';

class FoodDiaryScreen extends StatefulWidget {
  final AnimationController animationController;

  const FoodDiaryScreen({Key key, this.animationController}) : super(key: key);
  @override
  _FoodDiaryScreenState createState() => _FoodDiaryScreenState();
}

class _FoodDiaryScreenState extends State<FoodDiaryScreen>
    with TickerProviderStateMixin {
  List<Widget> listViews = List<Widget>();

  @override
  void initState() {
    addAllListData();
    super.initState();
  }

  void addAllListData() {
    var count = 9;
    listViews.add(
      TitleView(
        titleTxt: 'Prescribed diet',
        subTxt: 'Details',
        animation: Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
            parent: widget.animationController,
            curve:
                Interval((1 / count) * 0, 1.0, curve: Curves.fastOutSlowIn))),
        animationController: widget.animationController,
        navigationAction: () => widget.animationController.reverse().then(
          (data) {
            if (!mounted) return;
            BlocProvider.of<HomeBloc>(context).add(
              NavigateToCalorieTracker(),
            );
            NavigatorProvider.of(context).service.currentPage =
                CalorieTrackerScreen(
              animationController: widget.animationController,
            );
          },
        ),
      ),
    );
    listViews.add(
      PrescribedDietView(
        animation: Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
            parent: widget.animationController,
            curve:
                Interval((1 / count) * 1, 1.0, curve: Curves.fastOutSlowIn))),
        animationController: widget.animationController,
      ),
    );
    listViews.add(
      TitleView(
        titleTxt: 'Meals today',
        subTxt: 'Customize',
        animation: Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
            parent: widget.animationController,
            curve:
                Interval((1 / count) * 2, 1.0, curve: Curves.fastOutSlowIn))),
        animationController: widget.animationController,
      ),
    );
    listViews.add(
      MealsListView(
        mainScreenAnimation: Tween(begin: 0.0, end: 1.0).animate(
            CurvedAnimation(
                parent: widget.animationController,
                curve: Interval((1 / count) * 3, 1.0,
                    curve: Curves.fastOutSlowIn))),
        mainScreenAnimationController: widget.animationController,
      ),
    );
    listViews.add(
      TitleView(
        titleTxt: 'Body measurement',
        subTxt: 'Today',
        animation: Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
            parent: widget.animationController,
            curve:
                Interval((1 / count) * 4, 1.0, curve: Curves.fastOutSlowIn))),
        animationController: widget.animationController,
      ),
    );

    listViews.add(
      BodyMeasurementView(
        animation: Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
            parent: widget.animationController,
            curve:
                Interval((1 / count) * 5, 1.0, curve: Curves.fastOutSlowIn))),
        animationController: widget.animationController,
      ),
    );
    listViews.add(
      TitleView(
        titleTxt: 'Water',
        subTxt: 'Aqua SmartBottle',
        animation: Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
            parent: widget.animationController,
            curve:
                Interval((1 / count) * 6, 1.0, curve: Curves.fastOutSlowIn))),
        animationController: widget.animationController,
        navigationAction: () => widget.animationController.reverse().then(
          (data) {
            if (!mounted) return;
            BlocProvider.of<HomeBloc>(context).add(NavigateToWaterDiary());
            NavigatorProvider.of(context).service.currentPage =
                WaterDiaryWidget();
          },
        ),
      ),
    );
    listViews.add(
      WaterView(
        mainScreenAnimation: Tween(begin: 0.0, end: 1.0).animate(
            CurvedAnimation(
                parent: widget.animationController,
                curve: Interval((1 / count) * 7, 1.0,
                    curve: Curves.fastOutSlowIn))),
        mainScreenAnimationController: widget.animationController,
      ),
    );
    listViews.add(
      GlassView(
        animation: Tween(begin: 0.0, end: 1.0).animate(
          CurvedAnimation(
            parent: widget.animationController,
            curve: Interval((1 / count) * 8, 1.0, curve: Curves.fastOutSlowIn),
          ),
        ),
        animationController: widget.animationController,
      ),
    );
  }

  Future<bool> getData() async {
    await Future.delayed(const Duration(milliseconds: 50));
    return true;
  }

  @override
  Widget build(BuildContext context) {
    if (!mounted) {
      CustomAppBar.title = "Liquid Diary";
    }
    return buildFoodDiaryPage();
  }

  Widget buildFoodDiaryPage() {
    return Container(
      color: DaintyColors.background,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Stack(
          children: <Widget>[
            getMainListViewUI(),
            SizedBox(
              height: MediaQuery.of(context).padding.bottom,
            )
          ],
        ),
      ),
    );
  }

  Widget getMainListViewUI() {
    return FutureBuilder(
      future: getData(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return SizedBox();
        } else {
          return ListView.builder(
            controller: NavigatorProvider.of(context).service.scrollController,
            padding: EdgeInsets.only(
              top: AppBar().preferredSize.height +
                  MediaQuery.of(context).padding.top +
                  24,
              bottom: 62 + MediaQuery.of(context).padding.bottom,
            ),
            itemCount: listViews.length,
            scrollDirection: Axis.vertical,
            itemBuilder: (context, index) {
              widget.animationController.forward();
              return listViews[index];
            },
          );
        }
      },
    );
  }
}
