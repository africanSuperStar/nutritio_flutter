import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class HomeEvent extends Equatable {
  HomeEvent([List props = const <dynamic>[]]) : super();
}

class NavigateToTherapeuticNutrition extends HomeEvent {
  @override
  String toString() => 'NavigateToTherapeuticNutrition';

  @override
  // TODO: implement props
  List<Object> get props => null;
}

class NavigateToFoodServiceAndAdministration extends HomeEvent {
  @override
  String toString() => 'NavigateToFoodServiceAndAdministration';

  @override
  // TODO: implement props
  List<Object> get props => null;
}

class NavigateToCommunityNutrition extends HomeEvent {
  @override
  String toString() => 'NavigateToCommunityNutrition';

  @override
  // TODO: implement props
  List<Object> get props => null;
}

class NavigateToFoodDiary extends HomeEvent {
  @override
  String toString() => 'NavigateToFoodDiary';

  @override
  // TODO: implement props
  List<Object> get props => null;
}

class NavigateToWaterDiary extends HomeEvent {
  @override
  String toString() => 'NavigateToWaterDiary';

  @override
  // TODO: implement props
  List<Object> get props => null;
}

class NavigateToTraining extends HomeEvent {
  @override
  String toString() => 'NavigateToTraining';

  @override
  // TODO: implement props
  List<Object> get props => null;
}

class NavigateToCalorieTracker extends HomeEvent {
  @override
  String toString() => 'NavigateToCalorieTracker';

  @override
  // TODO: implement props
  List<Object> get props => null;
}

class NavigateToSettings extends HomeEvent {
  @override
  String toString() => 'NavigateToSettings';

  @override
  // TODO: implement props
  List<Object> get props => null;
}
