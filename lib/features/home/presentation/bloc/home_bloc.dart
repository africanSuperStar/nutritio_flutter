import 'dart:async';

import 'package:bloc/bloc.dart';

import './bloc.dart';
import '../../../../models/meals/water.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  @override
  HomeState get initialState => InitialHomeState();

  Water _currentWaterEntry = Water(
    dailyGoal: 0,
    currentLevel: 0,
    lastDrink: "",
    timeToRefill: false,
  );

  @override
  Stream<HomeState> mapEventToState(
    HomeEvent event,
  ) async* {
    if (event is NavigateToTraining) {
      yield TrainingActive();
    } else if (event is NavigateToCalorieTracker) {
      yield CalorieTrackerActive();
    } else if (event is NavigateToWaterDiary) {
      yield WaterDiaryActive();
    } else if (event is NavigateToFoodDiary) {
      yield FoodDiaryActive(_currentWaterEntry);
    } else if (event is NavigateToTherapeuticNutrition) {
      yield TherapeuticNutritionActive();
    } else if (event is NavigateToFoodServiceAndAdministration) {
      yield FoodServiceAndAdministrationActive();
    } else if (event is NavigateToCommunityNutrition) {
      yield CommunityNutritionActive();
    } else if (event is NavigateToSettings) {
      yield SettingsActive();
    }
  }
}
