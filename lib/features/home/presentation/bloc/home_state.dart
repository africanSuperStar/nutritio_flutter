import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../../models/meals/water.dart';

@immutable
abstract class HomeState extends Equatable {
  HomeState([List props = const <dynamic>[]]) : super();
}

class InitialHomeState extends HomeState {
  @override
  List<Object> get props => null;
}

class SettingsActive extends HomeState {
  static final route = "/settings";
  final String title = "Settings";
  final int itemIndex = 0;

  @override
  List<Object> get props => null;
}

class WaterDiaryActive extends HomeState {
  static final route = "/water_diary";
  final String title = "Water Diary";

  @override
  List<Object> get props => null;
}

class FoodDiaryActive extends HomeState {
  final Water waterEntry;

  FoodDiaryActive(this.waterEntry) : super([waterEntry]);

  static final route = "/food_diary";
  final String title = "Meal Diary";
  final int itemIndex = 1;

  @override
  List<Object> get props => null;
}

class CalorieTrackerActive extends HomeState {
  static final route = "/calorie_tracker";
  final String title = "Calorie Tracker";

  @override
  List<Object> get props => null;
}

class TrainingActive extends HomeState {
  static final route = "/training";
  final String title = "Training";

  @override
  List<Object> get props => null;
}

class TherapeuticNutritionActive extends HomeState {
  static final route = "/therapeutic_nutrition";
  final String title = "Therapeutic Nutrition";
  final int itemIndex = 2;

  @override
  List<Object> get props => null;
}

class FoodServiceAndAdministrationActive extends HomeState {
  static final route = "/food_service_and_administration";
  final String title = "Food Service and Administration";
  final int itemIndex = 3;

  @override
  List<Object> get props => null;
}

class CommunityNutritionActive extends HomeState {
  static final route = "/community_nutrition";
  final String title = "Community Nutrition";
  final int itemIndex = 4;

  @override
  List<Object> get props => null;
}
