import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../models/meals/tab_icon_data.dart';
import '../../../../navigator_service.dart';
import '../../../food_diary/presentation/pages/food_diary_screen.dart';
import '../../../training_diary/presentation/pages/training_diary_screen.dart';
import '../bloc/bloc.dart';
import 'bottom_navigationbar_view.dart';

class BottomNavigationBarWidget extends StatefulWidget {
  final AnimationController animationController;

  BottomNavigationBarWidget({Key key, this.animationController})
      : super(key: key);

  @override
  _BottomNavigationBarWidgetState createState() =>
      _BottomNavigationBarWidgetState();
}

class _BottomNavigationBarWidgetState extends State<BottomNavigationBarWidget>
    with SingleTickerProviderStateMixin {
  List<TabIconData> tabIconsList = TabIconData.tabIconsList;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    NavigatorService _service = NavigatorProvider.of(context).service;

    return Column(
      children: <Widget>[
        Expanded(
          child: SizedBox(),
        ),
        BottomNavigationBarView(
          tabIconsList: tabIconsList,
          addClick: () {},
          changeIndex: (index) {
            if (index == 0) {
              widget.animationController.reverse().then((data) {
                if (!mounted) return;
                setState(() {
                  _service.currentPage = FoodDiaryScreen(
                    animationController: widget.animationController,
                  );
                  BlocProvider.of<HomeBloc>(context)
                      .add(NavigateToFoodDiary());
                });
              });
            } else if (index == 1) {
              widget.animationController.reverse().then((data) {
                if (!mounted) return;
                setState(() {
                  _service.currentPage = TrainingScreen(
                    animationController: widget.animationController,
                  );
                  BlocProvider.of<HomeBloc>(context)
                      .add(NavigateToTraining());
                });
              });
            }
          },
        ),
      ],
    );
  }
}
