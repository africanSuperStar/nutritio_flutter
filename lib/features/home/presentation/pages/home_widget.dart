import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../auth/presentation/bloc/bloc.dart';
import '../../../water_diary/presentation/bloc/bloc.dart';
import '../bloc/bloc.dart';
import 'home_screen.dart';

class HomeWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: null,
      body: MultiBlocProvider(
        providers: [
          BlocProvider<HomeBloc>(
            builder: (context) => HomeBloc(),
          ),
          BlocProvider<AuthBloc>(
            builder: (context) => AuthBloc(),
          ),
          BlocProvider<WaterDiaryBloc>(
            builder: (context) => WaterDiaryBloc(),
          ),
        ],
        child: HomeScreen(),
      ),
    );
  }
}
