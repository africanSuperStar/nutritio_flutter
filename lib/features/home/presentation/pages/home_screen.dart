import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../navigator_service.dart';
import '../../../../utils/colors.dart';
import '../../../auth/presentation/bloc/bloc.dart';
import '../../../food_diary/presentation/pages/food_diary_screen.dart';
import '../bloc/bloc.dart';
import '../widgets/bottom_navigationbar_widget.dart';
import '../widgets/custom_appbar.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  AuthBloc _authBloc;
  HomeBloc _homeBloc;
  CustomAppBar _customAppBar;

  AnimationController _animationController;

  NavigatorService _service;
  BottomNavigationBarWidget _bottomBar;

  @override
  initState() {
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 600),
    );
    _homeBloc = BlocProvider.of<HomeBloc>(context);
    _authBloc = BlocProvider.of<AuthBloc>(context);
    _customAppBar = CustomAppBar(
      animationController: _animationController,
    );
    _bottomBar = BottomNavigationBarWidget(
      animationController: _animationController,
    );
    super.initState();
  }

  @override
  void dispose() {
    _homeBloc.close();
    _authBloc.close();
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _service = NavigatorProvider.of(context).service;

    _service.currentPage = FoodDiaryScreen(
      animationController: _animationController,
    );

    return BlocBuilder(
      bloc: _homeBloc,
      builder: (BuildContext context, HomeState state) {
        return buildHomePage(context);
      },
    );
  }

  Widget buildHomePage(BuildContext context) {
    return Scaffold(
      backgroundColor: DaintyColors.background,
      body: FutureBuilder(
        future: getData(),
        builder: (context, snapshot) {
          return Stack(
            children: <Widget>[
              _service.currentPage,
              _customAppBar,
              _bottomBar,
            ],
          );
        },
      ),
    );
  }

  Future<bool> getData() async {
    await Future.delayed(const Duration(milliseconds: 200));
    return true;
  }
}
