import 'package:flutter/material.dart';
import 'package:nutritio/features/auth/presentation/widgets/auth_widget.dart';
import 'package:nutritio/features/auth/presentation/widgets/forgot_password/forgot_password_widget.dart';
import 'package:nutritio/features/auth/presentation/widgets/registration/registration_widget.dart';
import 'package:nutritio/features/home/presentation/pages/home_widget.dart';
import 'package:nutritio/features/settings/presentation/pages/settings_screen.dart';

Map<String, WidgetBuilder> routes = {
  '/login': (context) => AuthenticationWidget(),
  '/registration': (context) => RegistrationWidget(),
  '/forgot-password': (context) => ForgotPasswordWidget(),
  '/home': (context) => HomeWidget(),
  '/settings': (context) => SettingsScreen(),
  // TO BE ADDED...
};
