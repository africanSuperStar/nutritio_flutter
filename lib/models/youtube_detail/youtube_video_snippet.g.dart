// GENERATED CODE - DO NOT MODIFY BY HAND

part of youtube_video_snippet;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<YoutubeVideoSnippet> _$youtubeVideoSnippetSerializer =
    new _$YoutubeVideoSnippetSerializer();

class _$YoutubeVideoSnippetSerializer
    implements StructuredSerializer<YoutubeVideoSnippet> {
  @override
  final Iterable<Type> types = const [
    YoutubeVideoSnippet,
    _$YoutubeVideoSnippet
  ];
  @override
  final String wireName = 'YoutubeVideoSnippet';

  @override
  Iterable<Object> serialize(
      Serializers serializers, YoutubeVideoSnippet object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'publishedAt',
      serializers.serialize(object.publishedAt,
          specifiedType: const FullType(String)),
      'channelId',
      serializers.serialize(object.channelId,
          specifiedType: const FullType(String)),
      'title',
      serializers.serialize(object.title,
          specifiedType: const FullType(String)),
      'description',
      serializers.serialize(object.description,
          specifiedType: const FullType(String)),
      'thumbnails',
      serializers.serialize(object.thumbnails,
          specifiedType: const FullType(Thumbnails)),
      'channelTitle',
      serializers.serialize(object.channelTitle,
          specifiedType: const FullType(String)),
      'tags',
      serializers.serialize(object.tags,
          specifiedType:
              const FullType(BuiltList, const [const FullType(String)])),
    ];

    return result;
  }

  @override
  YoutubeVideoSnippet deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new YoutubeVideoSnippetBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'publishedAt':
          result.publishedAt = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'channelId':
          result.channelId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'thumbnails':
          result.thumbnails.replace(serializers.deserialize(value,
              specifiedType: const FullType(Thumbnails)) as Thumbnails);
          break;
        case 'channelTitle':
          result.channelTitle = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'tags':
          result.tags.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(String)]))
              as BuiltList<dynamic>);
          break;
      }
    }

    return result.build();
  }
}

class _$YoutubeVideoSnippet extends YoutubeVideoSnippet {
  @override
  final String publishedAt;
  @override
  final String channelId;
  @override
  final String title;
  @override
  final String description;
  @override
  final Thumbnails thumbnails;
  @override
  final String channelTitle;
  @override
  final BuiltList<String> tags;

  factory _$YoutubeVideoSnippet(
          [void Function(YoutubeVideoSnippetBuilder) updates]) =>
      (new YoutubeVideoSnippetBuilder()..update(updates)).build();

  _$YoutubeVideoSnippet._(
      {this.publishedAt,
      this.channelId,
      this.title,
      this.description,
      this.thumbnails,
      this.channelTitle,
      this.tags})
      : super._() {
    if (publishedAt == null) {
      throw new BuiltValueNullFieldError('YoutubeVideoSnippet', 'publishedAt');
    }
    if (channelId == null) {
      throw new BuiltValueNullFieldError('YoutubeVideoSnippet', 'channelId');
    }
    if (title == null) {
      throw new BuiltValueNullFieldError('YoutubeVideoSnippet', 'title');
    }
    if (description == null) {
      throw new BuiltValueNullFieldError('YoutubeVideoSnippet', 'description');
    }
    if (thumbnails == null) {
      throw new BuiltValueNullFieldError('YoutubeVideoSnippet', 'thumbnails');
    }
    if (channelTitle == null) {
      throw new BuiltValueNullFieldError('YoutubeVideoSnippet', 'channelTitle');
    }
    if (tags == null) {
      throw new BuiltValueNullFieldError('YoutubeVideoSnippet', 'tags');
    }
  }

  @override
  YoutubeVideoSnippet rebuild(
          void Function(YoutubeVideoSnippetBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  YoutubeVideoSnippetBuilder toBuilder() =>
      new YoutubeVideoSnippetBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is YoutubeVideoSnippet &&
        publishedAt == other.publishedAt &&
        channelId == other.channelId &&
        title == other.title &&
        description == other.description &&
        thumbnails == other.thumbnails &&
        channelTitle == other.channelTitle &&
        tags == other.tags;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, publishedAt.hashCode), channelId.hashCode),
                        title.hashCode),
                    description.hashCode),
                thumbnails.hashCode),
            channelTitle.hashCode),
        tags.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('YoutubeVideoSnippet')
          ..add('publishedAt', publishedAt)
          ..add('channelId', channelId)
          ..add('title', title)
          ..add('description', description)
          ..add('thumbnails', thumbnails)
          ..add('channelTitle', channelTitle)
          ..add('tags', tags))
        .toString();
  }
}

class YoutubeVideoSnippetBuilder
    implements Builder<YoutubeVideoSnippet, YoutubeVideoSnippetBuilder> {
  _$YoutubeVideoSnippet _$v;

  String _publishedAt;
  String get publishedAt => _$this._publishedAt;
  set publishedAt(String publishedAt) => _$this._publishedAt = publishedAt;

  String _channelId;
  String get channelId => _$this._channelId;
  set channelId(String channelId) => _$this._channelId = channelId;

  String _title;
  String get title => _$this._title;
  set title(String title) => _$this._title = title;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  ThumbnailsBuilder _thumbnails;
  ThumbnailsBuilder get thumbnails =>
      _$this._thumbnails ??= new ThumbnailsBuilder();
  set thumbnails(ThumbnailsBuilder thumbnails) =>
      _$this._thumbnails = thumbnails;

  String _channelTitle;
  String get channelTitle => _$this._channelTitle;
  set channelTitle(String channelTitle) => _$this._channelTitle = channelTitle;

  ListBuilder<String> _tags;
  ListBuilder<String> get tags => _$this._tags ??= new ListBuilder<String>();
  set tags(ListBuilder<String> tags) => _$this._tags = tags;

  YoutubeVideoSnippetBuilder();

  YoutubeVideoSnippetBuilder get _$this {
    if (_$v != null) {
      _publishedAt = _$v.publishedAt;
      _channelId = _$v.channelId;
      _title = _$v.title;
      _description = _$v.description;
      _thumbnails = _$v.thumbnails?.toBuilder();
      _channelTitle = _$v.channelTitle;
      _tags = _$v.tags?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(YoutubeVideoSnippet other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$YoutubeVideoSnippet;
  }

  @override
  void update(void Function(YoutubeVideoSnippetBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$YoutubeVideoSnippet build() {
    _$YoutubeVideoSnippet _$result;
    try {
      _$result = _$v ??
          new _$YoutubeVideoSnippet._(
              publishedAt: publishedAt,
              channelId: channelId,
              title: title,
              description: description,
              thumbnails: thumbnails.build(),
              channelTitle: channelTitle,
              tags: tags.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'thumbnails';
        thumbnails.build();

        _$failedField = 'tags';
        tags.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'YoutubeVideoSnippet', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
