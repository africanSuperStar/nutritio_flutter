// GENERATED CODE - DO NOT MODIFY BY HAND

part of youtube_video_item;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<YoutubeVideoItem> _$youtubeVideoItemSerializer =
    new _$YoutubeVideoItemSerializer();

class _$YoutubeVideoItemSerializer
    implements StructuredSerializer<YoutubeVideoItem> {
  @override
  final Iterable<Type> types = const [YoutubeVideoItem, _$YoutubeVideoItem];
  @override
  final String wireName = 'YoutubeVideoItem';

  @override
  Iterable<Object> serialize(Serializers serializers, YoutubeVideoItem object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'snippet',
      serializers.serialize(object.snippet,
          specifiedType: const FullType(YoutubeVideoSnippet)),
    ];

    return result;
  }

  @override
  YoutubeVideoItem deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new YoutubeVideoItemBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'snippet':
          result.snippet.replace(serializers.deserialize(value,
                  specifiedType: const FullType(YoutubeVideoSnippet))
              as YoutubeVideoSnippet);
          break;
      }
    }

    return result.build();
  }
}

class _$YoutubeVideoItem extends YoutubeVideoItem {
  @override
  final String id;
  @override
  final YoutubeVideoSnippet snippet;

  factory _$YoutubeVideoItem(
          [void Function(YoutubeVideoItemBuilder) updates]) =>
      (new YoutubeVideoItemBuilder()..update(updates)).build();

  _$YoutubeVideoItem._({this.id, this.snippet}) : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('YoutubeVideoItem', 'id');
    }
    if (snippet == null) {
      throw new BuiltValueNullFieldError('YoutubeVideoItem', 'snippet');
    }
  }

  @override
  YoutubeVideoItem rebuild(void Function(YoutubeVideoItemBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  YoutubeVideoItemBuilder toBuilder() =>
      new YoutubeVideoItemBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is YoutubeVideoItem &&
        id == other.id &&
        snippet == other.snippet;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, id.hashCode), snippet.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('YoutubeVideoItem')
          ..add('id', id)
          ..add('snippet', snippet))
        .toString();
  }
}

class YoutubeVideoItemBuilder
    implements Builder<YoutubeVideoItem, YoutubeVideoItemBuilder> {
  _$YoutubeVideoItem _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  YoutubeVideoSnippetBuilder _snippet;
  YoutubeVideoSnippetBuilder get snippet =>
      _$this._snippet ??= new YoutubeVideoSnippetBuilder();
  set snippet(YoutubeVideoSnippetBuilder snippet) => _$this._snippet = snippet;

  YoutubeVideoItemBuilder();

  YoutubeVideoItemBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _snippet = _$v.snippet?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(YoutubeVideoItem other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$YoutubeVideoItem;
  }

  @override
  void update(void Function(YoutubeVideoItemBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$YoutubeVideoItem build() {
    _$YoutubeVideoItem _$result;
    try {
      _$result =
          _$v ?? new _$YoutubeVideoItem._(id: id, snippet: snippet.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'snippet';
        snippet.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'YoutubeVideoItem', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
