library youtube_video_snippet;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:nutritio/models/youtube_search/model.dart';
import 'package:nutritio/models/youtube_search/serializers.dart';

part 'youtube_video_snippet.g.dart';

abstract class YoutubeVideoSnippet implements Built<YoutubeVideoSnippet, YoutubeVideoSnippetBuilder> {
  String get publishedAt;
  String get channelId;
  String get title;
  String get description;
  Thumbnails get thumbnails;
  String get channelTitle;
  BuiltList<String> get tags;

  YoutubeVideoSnippet._();

  factory YoutubeVideoSnippet([updates(YoutubeVideoSnippetBuilder b)]) = _$YoutubeVideoSnippet;

  String toJson() {
    return json.encode(serializers.serializeWith(YoutubeVideoSnippet.serializer, this));
  }

  static YoutubeVideoSnippet fromJson(String jsonString) {
    return serializers.deserializeWith(YoutubeVideoSnippet.serializer, json.decode(jsonString));
  }

  static Serializer<YoutubeVideoSnippet> get serializer => _$youtubeVideoSnippetSerializer;
}
