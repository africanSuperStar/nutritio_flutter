library youtube_video_item;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:nutritio/models/youtube_detail/model.dart';
import 'package:nutritio/models/youtube_search/serializers.dart';

part 'youtube_video_item.g.dart';

abstract class YoutubeVideoItem implements Built<YoutubeVideoItem, YoutubeVideoItemBuilder> {
  String get id;
  YoutubeVideoSnippet get snippet;

  YoutubeVideoItem._();

  factory YoutubeVideoItem([updates(YoutubeVideoItemBuilder b)]) = _$YoutubeVideoItem;

  String toJson() {
    return json.encode(serializers.serializeWith(YoutubeVideoItem.serializer, this));
  }

  static YoutubeVideoItem fromJson(String jsonString) {
    return serializers.deserializeWith(YoutubeVideoItem.serializer, json.decode(jsonString));
  }

  static Serializer<YoutubeVideoItem> get serializer => _$youtubeVideoItemSerializer;
}
