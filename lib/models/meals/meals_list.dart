import 'package:meta/meta.dart';

class MealsList {
  int id;

  final String imagePath;
  final String titleText;
  final String startColor;
  final String endColor;
  final List<String> meals;
  final int kacl;

  MealsList({
    this.imagePath = '',
    this.titleText = '',
    this.startColor = "",
    this.endColor = "",
    @required this.meals,
    this.kacl = 0,
  });

  Map<String, dynamic> toMap() {
    return {
      'imagePath': imagePath,
      'titleText': titleText,
      'startColor': startColor,
      'endColor': endColor,
      'meals': meals,
      'kacl': kacl,
    };
  }

  static MealsList fromMap(Map<String, dynamic> map) {
    return MealsList(
      imagePath: map['imagePath'],
      titleText: map['titleText'],
      startColor: map['startColor'],
      endColor: map['endColor'],
      meals: map['meals'],
      kacl: map['kacl'],
    );
  }

  static List<MealsList> tabIconsList = [
    MealsList(
      imagePath: 'assets/images/fitness/breakfast.png',
      titleText: 'Breakfast',
      kacl: 525,
      meals: ["Bread,", "Peanut butter,", "Apple"],
      startColor: "#FA7D82",
      endColor: "#FFB295",
    ),
    MealsList(
      imagePath: 'assets/images/fitness/lunch.png',
      titleText: 'Lunch',
      kacl: 602,
      meals: ["Salmon,", "Mixed veggies,", "Avocado"],
      startColor: "#738AE6",
      endColor: "#5C5EDD",
    ),
    MealsList(
      imagePath: 'assets/images/fitness/snack.png',
      titleText: 'Snack',
      kacl: 0,
      meals: ["Recommend:", "800 kcal"],
      startColor: "#FE95B6",
      endColor: "#FF5287",
    ),
    MealsList(
      imagePath: 'assets/images/fitness/dinner.png',
      titleText: 'Dinner',
      kacl: 0,
      meals: ["Recommend:", "703 kcal"],
      startColor: "#6F72CA",
      endColor: "#1E1466",
    ),
  ];
}
