class Water {
  int id;

  final int dailyGoal;
  final int currentLevel;
  final String lastDrink;
  final bool timeToRefill;

  Water({
    this.dailyGoal = 0,
    this.currentLevel = 0,
    this.lastDrink = "",
    this.timeToRefill = false,
  });

  Map<String, dynamic> toMap() {
    return {
      'dailyGoal': dailyGoal,
      'currentLevel': currentLevel,
      'lastDrink': lastDrink,
      'timeToRefill': timeToRefill,
    };
  }

  static Water fromMap(Map<String, dynamic> map) {
    return Water(
      dailyGoal: map['dailyGoal'],
      currentLevel: map['currentLevel'],
      lastDrink: map['lastDrink'],
      timeToRefill: map['timeToRefill'],
    );
  }
}
