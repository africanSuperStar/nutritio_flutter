import 'package:flutter/material.dart';

class TabIconData {
  int id;

  int index;
  String imagePath;
  String selectedImagePath;
  bool isSelected;
  AnimationController animationController;

  TabIconData({
    this.index = 0,
    this.imagePath = '',
    this.selectedImagePath = "",
    this.isSelected = false,
    this.animationController,
  });

  static List<TabIconData> tabIconsList = [
    TabIconData(
      imagePath: 'assets/images/fitness/tab_1.png',
      selectedImagePath: 'assets/images/fitness/tab_1s.png',
      index: 0,
      isSelected: true,
      animationController: null,
    ),
    TabIconData(
      imagePath: 'assets/images/fitness/tab_2.png',
      selectedImagePath: 'assets/images/fitness/tab_2s.png',
      index: 1,
      isSelected: false,
      animationController: null,

    ),
    TabIconData(
      imagePath: 'assets/images/fitness/tab_3.png',
      selectedImagePath: 'assets/images/fitness/tab_3s.png',
      index: 2,
      isSelected: false,
      animationController: null,

    ),
    TabIconData(
      imagePath: 'assets/images/fitness/tab_4.png',
      selectedImagePath: 'assets/images/fitness/tab_4s.png',
      index: 3,
      isSelected: false,
      animationController: null,

    ),
  ];
}