import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:nutritio/models/paypal_oauth/model.dart';
import 'package:nutritio/models/paypal_oauth/paypal_auth_error.dart';
import 'package:nutritio/models/paypal_oauth/paypal_credentials.dart';

class PayPalDataSource {
  final _tokenBaseUrl = 'https://api.sandbox.paypal.com/v1';

  Future<PayPalTokenResult> receiveAccessToken() async {
    final urlRaw = _tokenBaseUrl + '/oauth2/token';
    final urlEncoded = Uri.parse(urlRaw);

    final queryParams = {"grant_type": "client_credentials"};

    final authToken = 'Basic ' + base64.encode(utf8.encode("${PAYPAL_CLIENT_ID}:${PAYPAL_CLIENT_SECRET}"));

    final response = await http.post(
      urlEncoded,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': authToken,
      },
      body: queryParams,
    );

    if (response.statusCode == 200) {
      return PayPalTokenResult.fromJson(response.body);
    } else {
      throw PayPalAuthError(json.decode(response.body)['message']);
    }
  }

  Future<PayPalProduct> createProduct({PayPalProductAbstractBuilder product, PayPalTokenResult token}) async {
    final urlRaw = _tokenBaseUrl + '/catalogs/products';
    final urlEncoded = Uri.parse(urlRaw);

    final queryParams = {
      'name': product.name,
      'description': product.description,
      'type': product.type,
      'category': product.category,
      'image_url': product.imageURL,
      'home_url': product.homeURL
    };

    final response = await http.post(
      urlEncoded,
      headers: {
        'Authorization': 'Bearer ${token.accessToken}',
        'Content-Type': 'application/json',
      },
      body: json.encode(queryParams),
    );

    if (response.statusCode == 201) {
      return PayPalProduct.fromJson(response.body);
    } else {
      print(json.encode(queryParams));
      throw PayPalAuthError(json.decode(response.body)['message']);
    }
  }

  Future<PayPalProductList> listProducts({PayPalTokenResult token}) async {
    final queryParams = {
      'page_size': 2,
      'page': 1,
      'total_required': true,
    };

    final urlRaw = _tokenBaseUrl +
        '/catalogs/products' +
        '?page_size=${queryParams['page_size']}' +
        '&page=${queryParams['page']}' +
        '&total_required=${queryParams['total_required']}';

    final urlEncoded = Uri.parse(urlRaw);

    final response = await http.get(
      urlEncoded,
      headers: {
        'Authorization': 'Bearer ${token.accessToken}',
        'Content-Type': 'application/json',
      },
    );

    if (response.statusCode == 200) {
      return PayPalProductList.fromJson(response.body);
    } else {
      print(json.encode(queryParams));
      throw PayPalAuthError(json.decode(response.body)['message']);
    }
  }
}
