class PayPalAuthError implements Exception {
  final String message;
  PayPalAuthError(this.message);
}
