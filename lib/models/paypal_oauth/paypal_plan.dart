library paypal_plan;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:nutritio/models/paypal_oauth/serializers.dart';

part 'paypal_plan.g.dart';

abstract class PayPalPlan implements Built<PayPalPlan, PayPalPlanBuilder> {
  PayPalPlan._();

  factory PayPalPlan([updates(PayPalPlanBuilder b)]) = _$PayPalPlan;

  String toJson() {
    return json.encode(serializers.serializeWith(PayPalPlan.serializer, this));
  }

  static PayPalPlan fromJson(String jsonString) {
    return serializers.deserializeWith(PayPalPlan.serializer, json.decode(jsonString));
  }

  static Serializer<PayPalPlan> get serializer => _$payPalPlanSerializer;
}
