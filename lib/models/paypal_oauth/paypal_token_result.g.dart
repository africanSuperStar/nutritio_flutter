// GENERATED CODE - DO NOT MODIFY BY HAND

part of paypal_token_result;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<PayPalTokenResult> _$payPalTokenResultSerializer =
    new _$PayPalTokenResultSerializer();

class _$PayPalTokenResultSerializer
    implements StructuredSerializer<PayPalTokenResult> {
  @override
  final Iterable<Type> types = const [PayPalTokenResult, _$PayPalTokenResult];
  @override
  final String wireName = 'PayPalTokenResult';

  @override
  Iterable<Object> serialize(Serializers serializers, PayPalTokenResult object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'access_token',
      serializers.serialize(object.accessToken,
          specifiedType: const FullType(String)),
      'token_type',
      serializers.serialize(object.tokenType,
          specifiedType: const FullType(String)),
      'app_id',
      serializers.serialize(object.appId,
          specifiedType: const FullType(String)),
      'expires_in',
      serializers.serialize(object.expiryDate,
          specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  PayPalTokenResult deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new PayPalTokenResultBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'access_token':
          result.accessToken = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'token_type':
          result.tokenType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'app_id':
          result.appId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'expires_in':
          result.expiryDate = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$PayPalTokenResult extends PayPalTokenResult {
  @override
  final String accessToken;
  @override
  final String tokenType;
  @override
  final String appId;
  @override
  final int expiryDate;

  factory _$PayPalTokenResult(
          [void Function(PayPalTokenResultBuilder) updates]) =>
      (new PayPalTokenResultBuilder()..update(updates)).build();

  _$PayPalTokenResult._(
      {this.accessToken, this.tokenType, this.appId, this.expiryDate})
      : super._() {
    if (accessToken == null) {
      throw new BuiltValueNullFieldError('PayPalTokenResult', 'accessToken');
    }
    if (tokenType == null) {
      throw new BuiltValueNullFieldError('PayPalTokenResult', 'tokenType');
    }
    if (appId == null) {
      throw new BuiltValueNullFieldError('PayPalTokenResult', 'appId');
    }
    if (expiryDate == null) {
      throw new BuiltValueNullFieldError('PayPalTokenResult', 'expiryDate');
    }
  }

  @override
  PayPalTokenResult rebuild(void Function(PayPalTokenResultBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PayPalTokenResultBuilder toBuilder() =>
      new PayPalTokenResultBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PayPalTokenResult &&
        accessToken == other.accessToken &&
        tokenType == other.tokenType &&
        appId == other.appId &&
        expiryDate == other.expiryDate;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, accessToken.hashCode), tokenType.hashCode),
            appId.hashCode),
        expiryDate.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PayPalTokenResult')
          ..add('accessToken', accessToken)
          ..add('tokenType', tokenType)
          ..add('appId', appId)
          ..add('expiryDate', expiryDate))
        .toString();
  }
}

class PayPalTokenResultBuilder
    implements Builder<PayPalTokenResult, PayPalTokenResultBuilder> {
  _$PayPalTokenResult _$v;

  String _accessToken;
  String get accessToken => _$this._accessToken;
  set accessToken(String accessToken) => _$this._accessToken = accessToken;

  String _tokenType;
  String get tokenType => _$this._tokenType;
  set tokenType(String tokenType) => _$this._tokenType = tokenType;

  String _appId;
  String get appId => _$this._appId;
  set appId(String appId) => _$this._appId = appId;

  int _expiryDate;
  int get expiryDate => _$this._expiryDate;
  set expiryDate(int expiryDate) => _$this._expiryDate = expiryDate;

  PayPalTokenResultBuilder();

  PayPalTokenResultBuilder get _$this {
    if (_$v != null) {
      _accessToken = _$v.accessToken;
      _tokenType = _$v.tokenType;
      _appId = _$v.appId;
      _expiryDate = _$v.expiryDate;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PayPalTokenResult other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$PayPalTokenResult;
  }

  @override
  void update(void Function(PayPalTokenResultBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$PayPalTokenResult build() {
    final _$result = _$v ??
        new _$PayPalTokenResult._(
            accessToken: accessToken,
            tokenType: tokenType,
            appId: appId,
            expiryDate: expiryDate);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
