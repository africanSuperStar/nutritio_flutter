library serializers;

import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:nutritio/models/paypal_oauth/model.dart';

part 'serializers.g.dart';

@SerializersFor(const [
  PayPalTokenResult,
  PayPalProduct,
  PayPalProductList,
  PayPalLink,
])
final Serializers serializers = (_$serializers.toBuilder()..addPlugin(StandardJsonPlugin())).build();
