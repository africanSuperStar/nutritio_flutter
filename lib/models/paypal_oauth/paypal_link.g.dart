// GENERATED CODE - DO NOT MODIFY BY HAND

part of paypal_link;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<PayPalLink> _$payPalLinkSerializer = new _$PayPalLinkSerializer();

class _$PayPalLinkSerializer implements StructuredSerializer<PayPalLink> {
  @override
  final Iterable<Type> types = const [PayPalLink, _$PayPalLink];
  @override
  final String wireName = 'PayPalLink';

  @override
  Iterable<Object> serialize(Serializers serializers, PayPalLink object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'href',
      serializers.serialize(object.href, specifiedType: const FullType(String)),
      'rel',
      serializers.serialize(object.rel, specifiedType: const FullType(String)),
      'method',
      serializers.serialize(object.method,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  PayPalLink deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new PayPalLinkBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'href':
          result.href = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'rel':
          result.rel = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'method':
          result.method = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$PayPalLink extends PayPalLink {
  @override
  final String href;
  @override
  final String rel;
  @override
  final String method;

  factory _$PayPalLink([void Function(PayPalLinkBuilder) updates]) =>
      (new PayPalLinkBuilder()..update(updates)).build();

  _$PayPalLink._({this.href, this.rel, this.method}) : super._() {
    if (href == null) {
      throw new BuiltValueNullFieldError('PayPalLink', 'href');
    }
    if (rel == null) {
      throw new BuiltValueNullFieldError('PayPalLink', 'rel');
    }
    if (method == null) {
      throw new BuiltValueNullFieldError('PayPalLink', 'method');
    }
  }

  @override
  PayPalLink rebuild(void Function(PayPalLinkBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PayPalLinkBuilder toBuilder() => new PayPalLinkBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PayPalLink &&
        href == other.href &&
        rel == other.rel &&
        method == other.method;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, href.hashCode), rel.hashCode), method.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PayPalLink')
          ..add('href', href)
          ..add('rel', rel)
          ..add('method', method))
        .toString();
  }
}

class PayPalLinkBuilder implements Builder<PayPalLink, PayPalLinkBuilder> {
  _$PayPalLink _$v;

  String _href;
  String get href => _$this._href;
  set href(String href) => _$this._href = href;

  String _rel;
  String get rel => _$this._rel;
  set rel(String rel) => _$this._rel = rel;

  String _method;
  String get method => _$this._method;
  set method(String method) => _$this._method = method;

  PayPalLinkBuilder();

  PayPalLinkBuilder get _$this {
    if (_$v != null) {
      _href = _$v.href;
      _rel = _$v.rel;
      _method = _$v.method;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PayPalLink other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$PayPalLink;
  }

  @override
  void update(void Function(PayPalLinkBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$PayPalLink build() {
    final _$result =
        _$v ?? new _$PayPalLink._(href: href, rel: rel, method: method);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
