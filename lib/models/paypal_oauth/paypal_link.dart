library paypal_link;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:nutritio/models/paypal_oauth/serializers.dart';

part 'paypal_link.g.dart';

abstract class PayPalLink implements Built<PayPalLink, PayPalLinkBuilder> {
  String get href;
  String get rel;
  String get method;

  PayPalLink._();

  factory PayPalLink([updates(PayPalLinkBuilder b)]) = _$PayPalLink;

  String toJson() {
    return json.encode(serializers.serializeWith(PayPalLink.serializer, this));
  }

  static PayPalLink fromJson(String jsonString) {
    return serializers.deserializeWith(PayPalLink.serializer, json.decode(jsonString));
  }

  static Serializer<PayPalLink> get serializer => _$payPalLinkSerializer;
}
