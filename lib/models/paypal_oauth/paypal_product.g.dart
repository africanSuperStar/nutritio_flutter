// GENERATED CODE - DO NOT MODIFY BY HAND

part of paypal_product;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<PayPalProduct> _$payPalProductSerializer =
    new _$PayPalProductSerializer();

class _$PayPalProductSerializer implements StructuredSerializer<PayPalProduct> {
  @override
  final Iterable<Type> types = const [PayPalProduct, _$PayPalProduct];
  @override
  final String wireName = 'PayPalProduct';

  @override
  Iterable<Object> serialize(Serializers serializers, PayPalProduct object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'description',
      serializers.serialize(object.description,
          specifiedType: const FullType(String)),
      'links',
      serializers.serialize(object.links,
          specifiedType:
              const FullType(BuiltList, const [const FullType(PayPalLink)])),
    ];
    if (object.type != null) {
      result
        ..add('type')
        ..add(serializers.serialize(object.type,
            specifiedType: const FullType(BuiltValueEnum)));
    }
    if (object.category != null) {
      result
        ..add('category')
        ..add(serializers.serialize(object.category,
            specifiedType: const FullType(BuiltValueEnum)));
    }
    if (object.imageURL != null) {
      result
        ..add('image_url')
        ..add(serializers.serialize(object.imageURL,
            specifiedType: const FullType(String)));
    }
    if (object.homeURL != null) {
      result
        ..add('home_url')
        ..add(serializers.serialize(object.homeURL,
            specifiedType: const FullType(String)));
    }
    if (object.createTime != null) {
      result
        ..add('create_time')
        ..add(serializers.serialize(object.createTime,
            specifiedType: const FullType(String)));
    }
    if (object.updateTime != null) {
      result
        ..add('update_time')
        ..add(serializers.serialize(object.updateTime,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  PayPalProduct deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new PayPalProductBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'type':
          result.type = serializers.deserialize(value,
              specifiedType: const FullType(BuiltValueEnum)) as BuiltValueEnum;
          break;
        case 'category':
          result.category = serializers.deserialize(value,
              specifiedType: const FullType(BuiltValueEnum)) as BuiltValueEnum;
          break;
        case 'image_url':
          result.imageURL = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'home_url':
          result.homeURL = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'create_time':
          result.createTime = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'update_time':
          result.updateTime = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'links':
          result.links.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(PayPalLink)]))
              as BuiltList<dynamic>);
          break;
      }
    }

    return result.build();
  }
}

class _$PayPalProduct extends PayPalProduct {
  @override
  final String id;
  @override
  final String name;
  @override
  final String description;
  @override
  final BuiltValueEnum type;
  @override
  final BuiltValueEnum category;
  @override
  final String imageURL;
  @override
  final String homeURL;
  @override
  final String createTime;
  @override
  final String updateTime;
  @override
  final BuiltList<PayPalLink> links;

  factory _$PayPalProduct([void Function(PayPalProductBuilder) updates]) =>
      (new PayPalProductBuilder()..update(updates)).build();

  _$PayPalProduct._(
      {this.id,
      this.name,
      this.description,
      this.type,
      this.category,
      this.imageURL,
      this.homeURL,
      this.createTime,
      this.updateTime,
      this.links})
      : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('PayPalProduct', 'id');
    }
    if (name == null) {
      throw new BuiltValueNullFieldError('PayPalProduct', 'name');
    }
    if (description == null) {
      throw new BuiltValueNullFieldError('PayPalProduct', 'description');
    }
    if (links == null) {
      throw new BuiltValueNullFieldError('PayPalProduct', 'links');
    }
  }

  @override
  PayPalProduct rebuild(void Function(PayPalProductBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PayPalProductBuilder toBuilder() => new PayPalProductBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PayPalProduct &&
        id == other.id &&
        name == other.name &&
        description == other.description &&
        type == other.type &&
        category == other.category &&
        imageURL == other.imageURL &&
        homeURL == other.homeURL &&
        createTime == other.createTime &&
        updateTime == other.updateTime &&
        links == other.links;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc($jc($jc(0, id.hashCode), name.hashCode),
                                    description.hashCode),
                                type.hashCode),
                            category.hashCode),
                        imageURL.hashCode),
                    homeURL.hashCode),
                createTime.hashCode),
            updateTime.hashCode),
        links.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PayPalProduct')
          ..add('id', id)
          ..add('name', name)
          ..add('description', description)
          ..add('type', type)
          ..add('category', category)
          ..add('imageURL', imageURL)
          ..add('homeURL', homeURL)
          ..add('createTime', createTime)
          ..add('updateTime', updateTime)
          ..add('links', links))
        .toString();
  }
}

class PayPalProductBuilder
    implements Builder<PayPalProduct, PayPalProductBuilder> {
  _$PayPalProduct _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  BuiltValueEnum _type;
  BuiltValueEnum get type => _$this._type;
  set type(BuiltValueEnum type) => _$this._type = type;

  BuiltValueEnum _category;
  BuiltValueEnum get category => _$this._category;
  set category(BuiltValueEnum category) => _$this._category = category;

  String _imageURL;
  String get imageURL => _$this._imageURL;
  set imageURL(String imageURL) => _$this._imageURL = imageURL;

  String _homeURL;
  String get homeURL => _$this._homeURL;
  set homeURL(String homeURL) => _$this._homeURL = homeURL;

  String _createTime;
  String get createTime => _$this._createTime;
  set createTime(String createTime) => _$this._createTime = createTime;

  String _updateTime;
  String get updateTime => _$this._updateTime;
  set updateTime(String updateTime) => _$this._updateTime = updateTime;

  ListBuilder<PayPalLink> _links;
  ListBuilder<PayPalLink> get links =>
      _$this._links ??= new ListBuilder<PayPalLink>();
  set links(ListBuilder<PayPalLink> links) => _$this._links = links;

  PayPalProductBuilder();

  PayPalProductBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _name = _$v.name;
      _description = _$v.description;
      _type = _$v.type;
      _category = _$v.category;
      _imageURL = _$v.imageURL;
      _homeURL = _$v.homeURL;
      _createTime = _$v.createTime;
      _updateTime = _$v.updateTime;
      _links = _$v.links?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PayPalProduct other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$PayPalProduct;
  }

  @override
  void update(void Function(PayPalProductBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$PayPalProduct build() {
    _$PayPalProduct _$result;
    try {
      _$result = _$v ??
          new _$PayPalProduct._(
              id: id,
              name: name,
              description: description,
              type: type,
              category: category,
              imageURL: imageURL,
              homeURL: homeURL,
              createTime: createTime,
              updateTime: updateTime,
              links: links.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'links';
        links.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'PayPalProduct', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
