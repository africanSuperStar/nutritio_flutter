library paypal_token_result;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:nutritio/models/paypal_oauth/serializers.dart';

part 'paypal_token_result.g.dart';

abstract class PayPalTokenResult implements Built<PayPalTokenResult, PayPalTokenResultBuilder> {
  @BuiltValueField(wireName: 'access_token')
  String get accessToken;

  @BuiltValueField(wireName: 'token_type')
  String get tokenType;

  @BuiltValueField(wireName: 'app_id')
  String get appId;

  @BuiltValueField(wireName: 'expires_in')
  int get expiryDate;

  PayPalTokenResult._();

  factory PayPalTokenResult([updates(PayPalTokenResultBuilder b)]) = _$PayPalTokenResult;

  String toJson() {
    return json.encode(serializers.serializeWith(PayPalTokenResult.serializer, this));
  }

  static PayPalTokenResult fromJson(String jsonString) {
    return serializers.deserializeWith(PayPalTokenResult.serializer, json.decode(jsonString));
  }

  static Serializer<PayPalTokenResult> get serializer => _$payPalTokenResultSerializer;
}
