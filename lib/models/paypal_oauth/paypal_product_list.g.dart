// GENERATED CODE - DO NOT MODIFY BY HAND

part of paypal_product_list;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<PayPalProductList> _$payPalProductListSerializer =
    new _$PayPalProductListSerializer();

class _$PayPalProductListSerializer
    implements StructuredSerializer<PayPalProductList> {
  @override
  final Iterable<Type> types = const [PayPalProductList, _$PayPalProductList];
  @override
  final String wireName = 'PayPalProductList';

  @override
  Iterable<Object> serialize(Serializers serializers, PayPalProductList object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'products',
      serializers.serialize(object.products,
          specifiedType:
              const FullType(BuiltList, const [const FullType(PayPalProduct)])),
      'total_items',
      serializers.serialize(object.totalItems,
          specifiedType: const FullType(int)),
      'total_pages',
      serializers.serialize(object.totalPages,
          specifiedType: const FullType(int)),
      'links',
      serializers.serialize(object.links,
          specifiedType:
              const FullType(BuiltList, const [const FullType(PayPalLink)])),
    ];

    return result;
  }

  @override
  PayPalProductList deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new PayPalProductListBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'products':
          result.products.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(PayPalProduct)]))
              as BuiltList<dynamic>);
          break;
        case 'total_items':
          result.totalItems = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'total_pages':
          result.totalPages = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'links':
          result.links.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(PayPalLink)]))
              as BuiltList<dynamic>);
          break;
      }
    }

    return result.build();
  }
}

class _$PayPalProductList extends PayPalProductList {
  @override
  final BuiltList<PayPalProduct> products;
  @override
  final int totalItems;
  @override
  final int totalPages;
  @override
  final BuiltList<PayPalLink> links;

  factory _$PayPalProductList(
          [void Function(PayPalProductListBuilder) updates]) =>
      (new PayPalProductListBuilder()..update(updates)).build();

  _$PayPalProductList._(
      {this.products, this.totalItems, this.totalPages, this.links})
      : super._() {
    if (products == null) {
      throw new BuiltValueNullFieldError('PayPalProductList', 'products');
    }
    if (totalItems == null) {
      throw new BuiltValueNullFieldError('PayPalProductList', 'totalItems');
    }
    if (totalPages == null) {
      throw new BuiltValueNullFieldError('PayPalProductList', 'totalPages');
    }
    if (links == null) {
      throw new BuiltValueNullFieldError('PayPalProductList', 'links');
    }
  }

  @override
  PayPalProductList rebuild(void Function(PayPalProductListBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PayPalProductListBuilder toBuilder() =>
      new PayPalProductListBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PayPalProductList &&
        products == other.products &&
        totalItems == other.totalItems &&
        totalPages == other.totalPages &&
        links == other.links;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, products.hashCode), totalItems.hashCode),
            totalPages.hashCode),
        links.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PayPalProductList')
          ..add('products', products)
          ..add('totalItems', totalItems)
          ..add('totalPages', totalPages)
          ..add('links', links))
        .toString();
  }
}

class PayPalProductListBuilder
    implements Builder<PayPalProductList, PayPalProductListBuilder> {
  _$PayPalProductList _$v;

  ListBuilder<PayPalProduct> _products;
  ListBuilder<PayPalProduct> get products =>
      _$this._products ??= new ListBuilder<PayPalProduct>();
  set products(ListBuilder<PayPalProduct> products) =>
      _$this._products = products;

  int _totalItems;
  int get totalItems => _$this._totalItems;
  set totalItems(int totalItems) => _$this._totalItems = totalItems;

  int _totalPages;
  int get totalPages => _$this._totalPages;
  set totalPages(int totalPages) => _$this._totalPages = totalPages;

  ListBuilder<PayPalLink> _links;
  ListBuilder<PayPalLink> get links =>
      _$this._links ??= new ListBuilder<PayPalLink>();
  set links(ListBuilder<PayPalLink> links) => _$this._links = links;

  PayPalProductListBuilder();

  PayPalProductListBuilder get _$this {
    if (_$v != null) {
      _products = _$v.products?.toBuilder();
      _totalItems = _$v.totalItems;
      _totalPages = _$v.totalPages;
      _links = _$v.links?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PayPalProductList other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$PayPalProductList;
  }

  @override
  void update(void Function(PayPalProductListBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$PayPalProductList build() {
    _$PayPalProductList _$result;
    try {
      _$result = _$v ??
          new _$PayPalProductList._(
              products: products.build(),
              totalItems: totalItems,
              totalPages: totalPages,
              links: links.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'products';
        products.build();

        _$failedField = 'links';
        links.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'PayPalProductList', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
