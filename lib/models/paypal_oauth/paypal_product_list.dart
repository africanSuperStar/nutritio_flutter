library paypal_product_list;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:nutritio/models/paypal_oauth/model.dart';
import 'package:nutritio/models/paypal_oauth/serializers.dart';

part 'paypal_product_list.g.dart';

abstract class PayPalProductList implements Built<PayPalProductList, PayPalProductListBuilder> {
  BuiltList<PayPalProduct> get products;

  @BuiltValueField(wireName: 'total_items')
  int get totalItems;

  @BuiltValueField(wireName: 'total_pages')
  int get totalPages;

  BuiltList<PayPalLink> get links;

  PayPalProductList._();

  factory PayPalProductList([updates(PayPalProductListBuilder b)]) = _$PayPalProductList;

  String toJson() {
    return json.encode(serializers.serializeWith(PayPalProductList.serializer, this));
  }

  static PayPalProductList fromJson(String jsonString) {
    return serializers.deserializeWith(PayPalProductList.serializer, json.decode(jsonString));
  }

  static Serializer<PayPalProductList> get serializer => _$payPalProductListSerializer;
}
