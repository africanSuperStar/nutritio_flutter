library paypal_product;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:nutritio/models/paypal_oauth/model.dart';
import 'package:nutritio/models/paypal_oauth/serializers.dart';

part 'paypal_product.g.dart';

abstract class PayPalProduct implements Built<PayPalProduct, PayPalProductBuilder> {
  String get id;
  String get name;
  String get description;

  @nullable
  BuiltValueEnum get type;

  @nullable
  BuiltValueEnum get category;

  @nullable
  @BuiltValueField(wireName: 'image_url')
  String get imageURL;

  @nullable
  @BuiltValueField(wireName: 'home_url')
  String get homeURL;

  @nullable
  @BuiltValueField(wireName: 'create_time')
  String get createTime;

  @nullable
  @BuiltValueField(wireName: 'update_time')
  String get updateTime;
  
  BuiltList<PayPalLink> get links;

  PayPalProduct._();

  factory PayPalProduct([updates(PayPalProductBuilder b)]) = _$PayPalProduct;

  String toJson() {
    return json.encode(serializers.serializeWith(PayPalProduct.serializer, this));
  }

  static PayPalProduct fromJson(String jsonString) {
    return serializers.deserializeWith(PayPalProduct.serializer, json.decode(jsonString));
  }

  static Serializer<PayPalProduct> get serializer => _$payPalProductSerializer;
}
