// GENERATED CODE - DO NOT MODIFY BY HAND

part of paypal_plan;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<PayPalPlan> _$payPalPlanSerializer = new _$PayPalPlanSerializer();

class _$PayPalPlanSerializer implements StructuredSerializer<PayPalPlan> {
  @override
  final Iterable<Type> types = const [PayPalPlan, _$PayPalPlan];
  @override
  final String wireName = 'PayPalPlan';

  @override
  Iterable<Object> serialize(Serializers serializers, PayPalPlan object,
      {FullType specifiedType = FullType.unspecified}) {
    return <Object>[];
  }

  @override
  PayPalPlan deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    return new PayPalPlanBuilder().build();
  }
}

class _$PayPalPlan extends PayPalPlan {
  factory _$PayPalPlan([void Function(PayPalPlanBuilder) updates]) =>
      (new PayPalPlanBuilder()..update(updates)).build();

  _$PayPalPlan._() : super._();

  @override
  PayPalPlan rebuild(void Function(PayPalPlanBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PayPalPlanBuilder toBuilder() => new PayPalPlanBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PayPalPlan;
  }

  @override
  int get hashCode {
    return 417244496;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('PayPalPlan').toString();
  }
}

class PayPalPlanBuilder implements Builder<PayPalPlan, PayPalPlanBuilder> {
  _$PayPalPlan _$v;

  PayPalPlanBuilder();

  @override
  void replace(PayPalPlan other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$PayPalPlan;
  }

  @override
  void update(void Function(PayPalPlanBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$PayPalPlan build() {
    final _$result = _$v ?? new _$PayPalPlan._();
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
