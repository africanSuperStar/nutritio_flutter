// GENERATED CODE - DO NOT MODIFY BY HAND

part of youtube_search_snippet;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<YoutubeSearchSnippet> _$youtubeSearchSnippetSerializer =
    new _$YoutubeSearchSnippetSerializer();

class _$YoutubeSearchSnippetSerializer
    implements StructuredSerializer<YoutubeSearchSnippet> {
  @override
  final Iterable<Type> types = const [
    YoutubeSearchSnippet,
    _$YoutubeSearchSnippet
  ];
  @override
  final String wireName = 'YoutubeSearchSnippet';

  @override
  Iterable<Object> serialize(
      Serializers serializers, YoutubeSearchSnippet object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'publishedAt',
      serializers.serialize(object.publishedAt,
          specifiedType: const FullType(String)),
      'channelId',
      serializers.serialize(object.channelId,
          specifiedType: const FullType(String)),
      'title',
      serializers.serialize(object.title,
          specifiedType: const FullType(String)),
      'description',
      serializers.serialize(object.description,
          specifiedType: const FullType(String)),
      'thumbnails',
      serializers.serialize(object.thumbnails,
          specifiedType: const FullType(Thumbnails)),
      'channelTitle',
      serializers.serialize(object.channelTitle,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  YoutubeSearchSnippet deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new YoutubeSearchSnippetBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'publishedAt':
          result.publishedAt = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'channelId':
          result.channelId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'thumbnails':
          result.thumbnails.replace(serializers.deserialize(value,
              specifiedType: const FullType(Thumbnails)) as Thumbnails);
          break;
        case 'channelTitle':
          result.channelTitle = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$YoutubeSearchSnippet extends YoutubeSearchSnippet {
  @override
  final String publishedAt;
  @override
  final String channelId;
  @override
  final String title;
  @override
  final String description;
  @override
  final Thumbnails thumbnails;
  @override
  final String channelTitle;

  factory _$YoutubeSearchSnippet(
          [void Function(YoutubeSearchSnippetBuilder) updates]) =>
      (new YoutubeSearchSnippetBuilder()..update(updates)).build();

  _$YoutubeSearchSnippet._(
      {this.publishedAt,
      this.channelId,
      this.title,
      this.description,
      this.thumbnails,
      this.channelTitle})
      : super._() {
    if (publishedAt == null) {
      throw new BuiltValueNullFieldError('YoutubeSearchSnippet', 'publishedAt');
    }
    if (channelId == null) {
      throw new BuiltValueNullFieldError('YoutubeSearchSnippet', 'channelId');
    }
    if (title == null) {
      throw new BuiltValueNullFieldError('YoutubeSearchSnippet', 'title');
    }
    if (description == null) {
      throw new BuiltValueNullFieldError('YoutubeSearchSnippet', 'description');
    }
    if (thumbnails == null) {
      throw new BuiltValueNullFieldError('YoutubeSearchSnippet', 'thumbnails');
    }
    if (channelTitle == null) {
      throw new BuiltValueNullFieldError(
          'YoutubeSearchSnippet', 'channelTitle');
    }
  }

  @override
  YoutubeSearchSnippet rebuild(
          void Function(YoutubeSearchSnippetBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  YoutubeSearchSnippetBuilder toBuilder() =>
      new YoutubeSearchSnippetBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is YoutubeSearchSnippet &&
        publishedAt == other.publishedAt &&
        channelId == other.channelId &&
        title == other.title &&
        description == other.description &&
        thumbnails == other.thumbnails &&
        channelTitle == other.channelTitle;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, publishedAt.hashCode), channelId.hashCode),
                    title.hashCode),
                description.hashCode),
            thumbnails.hashCode),
        channelTitle.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('YoutubeSearchSnippet')
          ..add('publishedAt', publishedAt)
          ..add('channelId', channelId)
          ..add('title', title)
          ..add('description', description)
          ..add('thumbnails', thumbnails)
          ..add('channelTitle', channelTitle))
        .toString();
  }
}

class YoutubeSearchSnippetBuilder
    implements Builder<YoutubeSearchSnippet, YoutubeSearchSnippetBuilder> {
  _$YoutubeSearchSnippet _$v;

  String _publishedAt;
  String get publishedAt => _$this._publishedAt;
  set publishedAt(String publishedAt) => _$this._publishedAt = publishedAt;

  String _channelId;
  String get channelId => _$this._channelId;
  set channelId(String channelId) => _$this._channelId = channelId;

  String _title;
  String get title => _$this._title;
  set title(String title) => _$this._title = title;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  ThumbnailsBuilder _thumbnails;
  ThumbnailsBuilder get thumbnails =>
      _$this._thumbnails ??= new ThumbnailsBuilder();
  set thumbnails(ThumbnailsBuilder thumbnails) =>
      _$this._thumbnails = thumbnails;

  String _channelTitle;
  String get channelTitle => _$this._channelTitle;
  set channelTitle(String channelTitle) => _$this._channelTitle = channelTitle;

  YoutubeSearchSnippetBuilder();

  YoutubeSearchSnippetBuilder get _$this {
    if (_$v != null) {
      _publishedAt = _$v.publishedAt;
      _channelId = _$v.channelId;
      _title = _$v.title;
      _description = _$v.description;
      _thumbnails = _$v.thumbnails?.toBuilder();
      _channelTitle = _$v.channelTitle;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(YoutubeSearchSnippet other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$YoutubeSearchSnippet;
  }

  @override
  void update(void Function(YoutubeSearchSnippetBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$YoutubeSearchSnippet build() {
    _$YoutubeSearchSnippet _$result;
    try {
      _$result = _$v ??
          new _$YoutubeSearchSnippet._(
              publishedAt: publishedAt,
              channelId: channelId,
              title: title,
              description: description,
              thumbnails: thumbnails.build(),
              channelTitle: channelTitle);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'thumbnails';
        thumbnails.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'YoutubeSearchSnippet', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
