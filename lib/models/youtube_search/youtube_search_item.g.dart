// GENERATED CODE - DO NOT MODIFY BY HAND

part of youtube_search_item;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<YoutubeSearchItem> _$youtubeSearchItemSerializer =
    new _$YoutubeSearchItemSerializer();

class _$YoutubeSearchItemSerializer
    implements StructuredSerializer<YoutubeSearchItem> {
  @override
  final Iterable<Type> types = const [YoutubeSearchItem, _$YoutubeSearchItem];
  @override
  final String wireName = 'YoutubeSearchItem';

  @override
  Iterable<Object> serialize(Serializers serializers, YoutubeSearchItem object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(Id)),
      'snippet',
      serializers.serialize(object.snippet,
          specifiedType: const FullType(YoutubeSearchSnippet)),
    ];

    return result;
  }

  @override
  YoutubeSearchItem deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new YoutubeSearchItemBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id.replace(serializers.deserialize(value,
              specifiedType: const FullType(Id)) as Id);
          break;
        case 'snippet':
          result.snippet.replace(serializers.deserialize(value,
                  specifiedType: const FullType(YoutubeSearchSnippet))
              as YoutubeSearchSnippet);
          break;
      }
    }

    return result.build();
  }
}

class _$YoutubeSearchItem extends YoutubeSearchItem {
  @override
  final Id id;
  @override
  final YoutubeSearchSnippet snippet;

  factory _$YoutubeSearchItem(
          [void Function(YoutubeSearchItemBuilder) updates]) =>
      (new YoutubeSearchItemBuilder()..update(updates)).build();

  _$YoutubeSearchItem._({this.id, this.snippet}) : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('YoutubeSearchItem', 'id');
    }
    if (snippet == null) {
      throw new BuiltValueNullFieldError('YoutubeSearchItem', 'snippet');
    }
  }

  @override
  YoutubeSearchItem rebuild(void Function(YoutubeSearchItemBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  YoutubeSearchItemBuilder toBuilder() =>
      new YoutubeSearchItemBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is YoutubeSearchItem &&
        id == other.id &&
        snippet == other.snippet;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, id.hashCode), snippet.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('YoutubeSearchItem')
          ..add('id', id)
          ..add('snippet', snippet))
        .toString();
  }
}

class YoutubeSearchItemBuilder
    implements Builder<YoutubeSearchItem, YoutubeSearchItemBuilder> {
  _$YoutubeSearchItem _$v;

  IdBuilder _id;
  IdBuilder get id => _$this._id ??= new IdBuilder();
  set id(IdBuilder id) => _$this._id = id;

  YoutubeSearchSnippetBuilder _snippet;
  YoutubeSearchSnippetBuilder get snippet =>
      _$this._snippet ??= new YoutubeSearchSnippetBuilder();
  set snippet(YoutubeSearchSnippetBuilder snippet) => _$this._snippet = snippet;

  YoutubeSearchItemBuilder();

  YoutubeSearchItemBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id?.toBuilder();
      _snippet = _$v.snippet?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(YoutubeSearchItem other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$YoutubeSearchItem;
  }

  @override
  void update(void Function(YoutubeSearchItemBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$YoutubeSearchItem build() {
    _$YoutubeSearchItem _$result;
    try {
      _$result = _$v ??
          new _$YoutubeSearchItem._(id: id.build(), snippet: snippet.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'id';
        id.build();
        _$failedField = 'snippet';
        snippet.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'YoutubeSearchItem', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
