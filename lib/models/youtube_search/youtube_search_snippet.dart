library youtube_search_snippet;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:nutritio/models/youtube_search/model.dart';
import 'package:nutritio/models/youtube_search/serializers.dart';

part 'youtube_search_snippet.g.dart';

abstract class YoutubeSearchSnippet implements Built<YoutubeSearchSnippet, YoutubeSearchSnippetBuilder> {
  String get publishedAt;
  String get channelId;
  String get title;
  String get description;
  Thumbnails get thumbnails;
  String get channelTitle;

  YoutubeSearchSnippet._();

  factory YoutubeSearchSnippet([updates(YoutubeSearchSnippetBuilder b)]) = _$YoutubeSearchSnippet;

  String toJson() {
    return json.encode(serializers.serializeWith(YoutubeSearchSnippet.serializer, this));
  }

  static YoutubeSearchSnippet fromJson(String jsonString) {
    return serializers.deserializeWith(YoutubeSearchSnippet.serializer, json.decode(jsonString));
  }

  static Serializer<YoutubeSearchSnippet> get serializer => _$youtubeSearchSnippetSerializer;
}
