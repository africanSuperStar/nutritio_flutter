library youtube_search_item;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:nutritio/models/youtube_search/model.dart';
import 'package:nutritio/models/youtube_search/serializers.dart';

part 'youtube_search_item.g.dart';

abstract class YoutubeSearchItem implements Built<YoutubeSearchItem, YoutubeSearchItemBuilder> {
  Id get id;
  YoutubeSearchSnippet get snippet;

  YoutubeSearchItem._();

  factory YoutubeSearchItem([updates(YoutubeSearchItemBuilder b)]) = _$YoutubeSearchItem;

  String toJson() {
    return json.encode(serializers.serializeWith(YoutubeSearchItem.serializer, this));
  }

  static YoutubeSearchItem fromJson(String jsonString) {
    return serializers.deserializeWith(YoutubeSearchItem.serializer, json.decode(jsonString));
  }

  static Serializer<YoutubeSearchItem> get serializer => _$youtubeSearchItemSerializer;
}
