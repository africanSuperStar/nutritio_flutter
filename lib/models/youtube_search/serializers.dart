library serializers;

import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:nutritio/models/youtube_detail/model.dart';
import 'package:nutritio/models/youtube_search/model.dart';

part 'serializers.g.dart';

@SerializersFor(const [
  YoutubeSearchResult,
  YoutubeSearchItem,
  Id,
  YoutubeSearchSnippet,
  Thumbnails,
  Thumbnail,
  YoutubeVideoItem,
  YoutubeVideoSnippet,
])
final Serializers serializers = (_$serializers.toBuilder()..addPlugin(StandardJsonPlugin())).build();
