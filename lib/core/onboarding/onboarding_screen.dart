import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

final pages = [
  OnboardingScreenViewModel(
    const Color(0xFF004156),
    const Color(0xFFDFDFDF),
    'assets/images/onboarding/undraw_Chef_cu0r.svg',
    'Ready To Get Healthy',
    'Choose your dietician. Develop a meal plan. Be your best possible self.',
    Icons.blur_circular,
  ),
  OnboardingScreenViewModel(
    const Color(0xFFE4534D),
    const Color(0xFF0A0A0A),
    'assets/images/onboarding/undraw_energizer_2224.svg',
    'Set your Goals',
    'Track Your Meals. Choose your favourites. Keep focused. Achieve your life goals.',
    Icons.blur_circular,
  ),
  OnboardingScreenViewModel(
    const Color(0xFFFF682D),
    const Color(0xFF0A0A0A),
    'assets/images/onboarding/undraw_special_event_4aj8.svg',
    'Give yourself the time you need!',
    'Dieticians get a break from tedious menial tasks.',
    Icons.blur_circular,
  ),
];

class OnboardingScreen extends StatelessWidget {
  final OnboardingScreenViewModel viewModel;
  final double percentVisible;

  OnboardingScreen({
    this.viewModel,
    this.percentVisible = 1.0,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: viewModel.color,
      child: Opacity(
        opacity: percentVisible,
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Transform(
            transform: Matrix4.translationValues(0.0, 50.0 * (1.0 - percentVisible), 0.0),
            child: Padding(
              padding: EdgeInsets.only(bottom: 25.0),
              child: SvgPicture.asset(viewModel.heroAssetPath, width: 200.0, height: 200.0),
            ),
          ),
          Transform(
            transform: Matrix4.translationValues(0.0, 30.0 * (1.0 - percentVisible), 0.0),
            child: Padding(
              padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
              child: Text(
                viewModel.title,
                style: TextStyle(
                  color: viewModel.textColor,
                  fontSize: 30,
                  fontFamily: 'Pacifico',
                ),
              ),
            ),
          ),
          Transform(
            transform: Matrix4.translationValues(0.0, 30.0 * (1.0 - percentVisible), 0.0),
            child: Padding(
              padding: EdgeInsets.fromLTRB(25.0, 0, 25.0, 75.0),
              child: Column(
                children: <Widget>[
                  Text(
                    viewModel.body,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: viewModel.textColor,
                      fontSize: 20,
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ]),
      ),
    );
  }
}

class OnboardingScreenViewModel {
  final Color color;
  final Color textColor;
  final String heroAssetPath;
  final String title;
  final String body;
  final IconData iconAssetPath;

  OnboardingScreenViewModel(
    this.color,
    this.textColor,
    this.heroAssetPath,
    this.title,
    this.body,
    this.iconAssetPath,
  );
}
